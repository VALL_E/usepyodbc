#!/usr/bin/env python
import inspect
import os
import sys
import pyodbc


# !/usr/bin/env python
def get_script_dir(follow_symlinks=True):
    if getattr(sys, 'frozen', False):  # py2exe, PyInstaller, cx_Freeze
        path = os.path.abspath(sys.executable)
    else:
        path = inspect.getabsfile(get_script_dir)
    if follow_symlinks:
        path = os.path.realpath(path)
    return os.path.dirname(path)


# Searching file with name included substring "currentDB"
def get_current_DB_full_file_name(curent_path=""):
    for fn in os.listdir('.'):
        if os.path.isfile(fn):
            # print (fn)
            if fn.find("currentDB") >= 0 and fn.find("currentDB") != None:
                return fn


# srtSQL = """
# SELECT *
# FROM OUT_XB
# WHERE OUT_XB.ID_model Like ?;
# """
"""srtSQL = '''
TRANSFORM First(round(XB.Value_XB,2)) AS [First-Value_XB]
SELECT XB.ID_model, XB.period
FROM XB
WHERE XB.ID_model Like ?
GROUP BY XB.ID_model, XB.period
PIVOT XB.ID_XB;
'''"""
srtSQL = '''
SELECT *
FROM XB
WHERE XB.ID_model Like ?;
'''

# DELETE % FROM currentp;
srtSQL_del = """
DELETE * 
FROM currentp 
WHERE currentp.pOil_H Like ?;
"""

srtSQL_id = """
SELECT First(var.ID_model) AS [First-ID_model]
FROM var, weights, currentp
WHERE ((var.fSYSTEM)= ? ) And ((var.PROD_WELL_DIRECTION)= ? )
ORDER BY 
Min(Abs(weights.W_Oil_H*(currentp.pOil_H-Var.Oil_H)/currentp.pOil_H)), 
Min(Abs(weights.W_Gas_H*(currentp.pGas_H-Var.Gas_H)/currentp.pGas_H)), 
Min(Abs(weights.W_Water_H*(currentp.pWater_H-Var.Water_H)/currentp.pWater_H)), 
Min(Abs(weights.W_gamma_oil*(currentp.pgamma_oil-Var.gamma_oil)/currentp.pgamma_oil)), 
Min(Abs(weights.W_KX*(currentp.pKX-Var.KX)/currentp.pKX)), 
Min(Abs(weights.W_ANIZ*(currentp.pANIZ-Var.ANIZ)/currentp.pANIZ)), 
Min(Abs(weights.W_seam_formation*(currentp.pseam_formation-Var.seam_formation)/currentp.pseam_formation)), 
Min(Abs(weights.W_P_PI*(currentp.pP_PI-Var.P_Pl)/currentp.pP_PI)), 
Min(Abs(weights.W_T_PI*(currentp.pT_PI-Var.T_Pl)/currentp.pT_PI)), 
Min(Abs(weights.W_gamma_gas*(currentp.pgamma_gas-Var.gamma_gas)/currentp.pgamma_gas)), 
Min(Abs(weights.W_any_name*(currentp.pany_name-Var.any_name)/currentp.pany_name)), 
Min(Abs(weights.W_PORO*(currentp.pPORO-Var.PORO)/currentp.pPORO)), 
Min(Abs(weights.W_oilsat*(currentp.poilsat-Var.oilsat)/currentp.poilsat)), 
Min(Abs(weights.W_gassat*(currentp.pgassat-Var.gassat)/currentp.pgassat)), 
Min(Abs(weights.W_NTG*(currentp.pNTG-Var.NTG)/currentp.pNTG));
"""


class get_XB:
    """docstring"""

    def __init__(self):
        """Constructor"""
        try:
            self.__curdir = get_script_dir()
            self.__curDB = get_current_DB_full_file_name()
            self.__connstr = (
                    r"DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};"
                    r"DBQ=" + self.__curdir + "/" + self.__curDB + ";"
            )
            self.__cnxn = pyodbc.connect(self.__connstr)
            self.__cursor = self.__cnxn.cursor()
        except Exception as e:
            self.__cnxn.close()
            pyodbc.pooling = False
            return ("Exception:" + str(e))
            raise
        else:
            pass
        finally:
            pass

    # def __del__(self):
    def endXB(self):
        """Destructor"""
        self.__cnxn.commit()
        self.__cursor.close()
        # del __cursor
        self.__cnxn.close()
        pyodbc.pooling = False
        # print("База закрыта.")

    def do(self, strMASK, сsrtSQL=srtSQL):
        try:
            strMASK2 = strMASK.replace('*', '%')
            __cursor = self.__cursor.execute(сsrtSQL, strMASK2)
            return self.__cursor.fetchall()
        except Exception as e:
            self.__cnxn.close()
            pyodbc.pooling = False
            return ("Exception:" + str(e))
            raise
        else:
            pass
        finally:
            pass

    def set_allP(self, cOil_H, cGas_H, cWater_H, cgamma_oil, cKX, cANIZ, cseam_formation,
                 cP_PI, cT_PI, cgamma_gas, cany_name, cPORO, coilsat, cgassat, cNTG):
        try:
            self.__cursor.execute(srtSQL_del, '%')
            self.__cnxn.commit()
            params = (cOil_H, cGas_H, cWater_H, cgamma_oil, cKX, cANIZ, cseam_formation,
                      cP_PI, cT_PI, cgamma_gas, cany_name, cPORO, coilsat, cgassat, cNTG)
            self.__cursor.execute("""insert into currentp(pOil_H, pGas_H, pWater_H, pgamma_oil, pKX, pANIZ, pseam_formation,
                                     pP_PI, pT_PI, pgamma_gas, pany_name, pPORO, poilsat, pgassat, pNTG) 
                                     values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""", params)
            self.__cnxn.commit()
        except Exception as e:
            self.__cnxn.close()
            pyodbc.pooling = False
            return ("Exception:" + str(e))
            raise
        else:
            pass
        finally:
            pass

    def get_id_XB(self, fSYSTEM, PROD_WELL_DIRECTION):
        try:
            params = (fSYSTEM, PROD_WELL_DIRECTION)
            self.__cursor.execute(srtSQL_id, params)
            return self.__cursor.fetchall()
        except Exception as e:
            self.__cnxn.close()
            pyodbc.pooling = False
            return ("Exception:" + str(e))
            raise
        else:
            pass
        finally:
            pass

    #def get_cursor(self):
    #    return self.__cursor

    def get_curdir(self):
        return self.__curdir

    def get_curDB(self):
        return self.__curDB

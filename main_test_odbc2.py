import sys
import numpy as np
import csv
from useodbc import get_XB


# get an get_XB class Instance
ext = get_XB()

# mask for Like-operator
#strMASK = "202*"
cOil_H = 12 #12.0742268
cGas_H = 0.001
cWater_H = 0.001 #40
cgamma_oil = 0.86 #0.60886598
cKX = 120 #120.742268
cANIZ = 0.1 #0.100618557
cseam_formation = 1
cP_PI = 220 #121.3608247
cT_PI = 67 #87.41443299
cgamma_gas = 0.62 #0.623835052
cany_name = 120
cPORO = 0.2 #0.201237113
coilsat = 0.61 #0.608553608
cgassat = 0.645 #0.64719433
cNTG = 0.6 #0.60371134
ext.set_allP(cOil_H, cGas_H, cWater_H, cgamma_oil, cKX, cANIZ, cseam_formation,
             cP_PI, cT_PI, cgamma_gas, cany_name, cPORO, coilsat, cgassat, cNTG)
fSYSTEM = "без ППД"
PROD_WELL_DIRECTION = "ГС"
rows = ext.get_id_XB(fSYSTEM, PROD_WELL_DIRECTION)
for row in rows:
	strMASKarr = row
strMASK = strMASKarr[0]
print(strMASK)

# get parameterized list
rows = ext.do(strMASK)
# get current directory
cur_dir = ext.get_curdir()

# by numpy
OutFileFullName = cur_dir + "/OUT_XB_сводный1.csv"
np.savetxt(OutFileFullName, rows, delimiter=";", fmt='%s')#, header=header)

# by csv
OutFileFullName = cur_dir + "/OUT_XB_сводный2.csv"
myFile = open(OutFileFullName, 'w', newline='')
with myFile:
	writer = csv.writer(myFile, delimiter=';')
	writer.writerows(rows)

'''
try:
	pass
	cursor = ext.get_cursor()
	for row in cursor.description:
		print row[0]
except Exception as e:
	raise
    print ("Exception:" + str(e))
else:
	pass
finally:
	pass
'''


# print to console. No recomended!
#for row in rows:
#    print(row)

ext.endXB()

#!/usr/bin/env python
import inspect
import os
import sys
import pyodbc


# !/usr/bin/env python
def get_script_dir(follow_symlinks=True):
    if getattr(sys, 'frozen', False):  # py2exe, PyInstaller, cx_Freeze
        path = os.path.abspath(sys.executable)
    else:
        path = inspect.getabsfile(get_script_dir)
    if follow_symlinks:
        path = os.path.realpath(path)
    return os.path.dirname(path)


# Searching file with name included substring "currentDB"
def get_current_DB_full_file_name(curent_path=""):
    for fn in os.listdir('.'):
        if os.path.isfile(fn):
            # import numpy as np
            if fn.find("currentDB") >= 0 and fn.find("currentDB") != None:
                return fn

curdir = get_script_dir()
curDB = get_current_DB_full_file_name()
connstr = (
	r"DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};"
	r"DBQ=" + curdir + "/" + curDB + ";"
)
cnxn = pyodbc.connect(connstr)
cursor = cnxn.cursor()

rock = "abc"
fSYSTEM = "linear row"
PROD_WELL_DIRECTION = "ГС"

#WHERE ((var.ID_model) Like ((var.fSYSTEM)= 'linear row' ) And ((var.PROD_WELL_DIRECTION)= 'ГС' ))
"""srtSQL_id = '''
SELECT var.ID_model AS [First-ID_model]
FROM var, weights, currentp
WHERE ((var.fSYSTEM)= ? ) And ((var.PROD_WELL_DIRECTION)= ? ) And ((var.rock)= ? ) 
ORDER BY 
Abs(weights.W_Oil_H*(currentp.pOil_H-Var.Oil_H)/currentp.pOil_H), 
Abs(weights.W_Gas_H*(currentp.pGas_H-Var.Gas_H)/currentp.pGas_H), 
Abs(weights.W_Water_H*(currentp.pWater_H-Var.Water_H)/currentp.pWater_H), 
Abs(weights.W_gamma_oil*(currentp.pgamma_oil-Var.gamma_oil)/currentp.pgamma_oil), 
Abs(weights.W_KX*(currentp.pKX-Var.KX)/currentp.pKX), 
Abs(weights.W_ANIZ*(currentp.pANIZ-Var.ANIZ)/currentp.pANIZ), 
Abs(weights.W_seam_formation*(currentp.pseam_formation-Var.seam_formation)/currentp.pseam_formation), 
Abs(weights.W_P_PI*(currentp.pP_PI-Var.P_Pl)/currentp.pP_PI), 
Abs(weights.W_T_PI*(currentp.pT_PI-Var.T_Pl)/currentp.pT_PI), 
Abs(weights.W_gamma_gas*(currentp.pgamma_gas-Var.gamma_gas)/currentp.pgamma_gas), 
Abs(weights.W_any_name*(currentp.pany_name-Var.any_name)/currentp.pany_name), 
Abs(weights.W_PORO*(currentp.pPORO-Var.PORO)/currentp.pPORO), 
Abs(weights.W_oilsat*(currentp.poilsat-Var.oilsat)/currentp.poilsat), 
Abs(weights.W_gassat*(currentp.pgassat-Var.gassat)/currentp.pgassat), 
Abs(weights.W_NTG*(currentp.pNTG-Var.NTG)/currentp.pNTG);
'''"""

srtSQL_id = """
SELECT (var.ID_model) AS [First-ID_model]
FROM var, weights, currentp
WHERE ((var.rock)= ? ) And ((var.fSYSTEM)= ? )  And ((var.PROD_WELL_DIRECTION)= ? ) 
ORDER BY (
Abs(weights.W_Oil_H*(currentp.pOil_H-Var.Oil_H)/currentp.pOil_H)+ 
Abs(weights.W_Gas_H*(currentp.pGas_H-Var.Gas_H)/currentp.pGas_H)+  
Abs(weights.W_Water_H*(currentp.pWater_H-Var.Water_H)/currentp.pWater_H)+  
Abs(weights.W_gamma_oil*(currentp.pgamma_oil-Var.gamma_oil)/currentp.pgamma_oil)+  
Abs(weights.W_KX*(currentp.pKX-Var.KX)/currentp.pKX)+  
Abs(weights.W_ANIZ*(currentp.pANIZ-Var.ANIZ)/currentp.pANIZ)+  
Abs(weights.W_seam_formation*(currentp.pseam_formation-Var.seam_formation)/currentp.pseam_formation)+   
Abs(weights.W_P_PI*(currentp.pP_PI-Var.P_Pl)/currentp.pP_PI)+ 
Abs(weights.W_T_PI*(currentp.pT_PI-Var.T_Pl)/currentp.pT_PI)+  
Abs(weights.W_gamma_gas*(currentp.pgamma_gas-Var.gamma_gas)/currentp.pgamma_gas)+  
Abs(weights.W_any_name*(currentp.pany_name-Var.any_name)/currentp.pany_name)+  
Abs(weights.W_PORO*(currentp.pPORO-Var.PORO)/currentp.pPORO)+  
Abs(weights.W_oilsat*(currentp.poilsat-Var.oilsat)/currentp.poilsat)+  
Abs(weights.W_gassat*(currentp.pgassat-Var.gassat)/currentp.pgassat)+ 
Abs(weights.W_NTG*(currentp.pNTG-Var.NTG)/currentp.pNTG));
"""

try:
	pass
	cursor = cursor.execute(srtSQL_id, rock, fSYSTEM, PROD_WELL_DIRECTION)
	#cursor = cursor.execute(srtSQL_id)
	rows = cursor.fetchall()
except Exception as e:
	cnxn.close()
	pyodbc.pooling = False
	print("Ошибка:" + str(e))
	raise
else:
	pass
	for row in rows
:		print(row)
		strMASK = row
	print(strMASK[0])
finally:
	pass
	cnxn.close()
	pyodbc.pooling = False
	print("База закрыта.")
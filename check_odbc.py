#                              ##                              ##                              #
#                              ##                              ##                              #
##                            ####                            ####                            ##
##                            ####                            ####                            ##
####                        ########                        ########                        ####
####                        ########                        ########                        ####
#######                  ##############                  ##############                  #######
########                ################                ################                ########
##############   ##############################   #############################   ##############
################################################################################################
################################################################################################
###                                                                                          ###
#                             https://pypi.org/project/pyodbc/                                 #
###                         https://github.com/mkleehammer/pyodbc                            ###
################################################################################################
################################################################################################


import pyodbc

'''
en: The easiest way to check if one of the Microsoft Access ODBC drivers is 
    available to your Python environment (on Windows) is to do
ru: Самый простой способ проверить, доступен ли один из драйверов ODBC Microsoft Access 
    для вашей среды Python (в Windows), - это
'''
[x for x in pyodbc.drivers() if x.startswith('Microsoft Access Driver')]

"""
for x in pyodbc.drivers():
	if x.startswith('Microsoft Access Driver'):
		print('Done')
	else:
		print('None')
"""

'''
en: Here is an example of how to open an MS Access database:
ru: Вот пример того, как открыть базу данных MS Access:
'''
conn_str = (
    r'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};'
    r'DBQ=C:\Users\Valiu\OneDrive\Рабочий стол\IntHur реинкарнация\БД\DB_рез_расч_ГДМ_1.3.0_release.accdb;'
)
cnxn = pyodbc.connect(conn_str)
crsr = cnxn.cursor()
for table_info in crsr.tables(tableType='TABLE'):
    print(table_info.table_name)

##################################################################################################
##################################################################################################
###
#                               Getting started (Начиная)
###
##################################################################################################
##################################################################################################


##################################################################################################
#
# Connect to a Database (Подключиться к базе данных)
#
##################################################################################################

'''
en: Pass an ODBC connection string to the pyodbc connect() function which will return a Connection. 
    Once you have a connection you can ask it for a Cursor. For example:
ru: Передайте строку соединения ODBC в функцию pyodbc connect (), которая вернет соединение. 
    Если у вас есть соединение, вы можете попросить его для курсора. Например:
'''
# Specifying the ODBC driver, server name, database, etc. directly
# Указание драйвера ODBC, имени сервера, базы данных и т. Д. Напрямую
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=testdb;UID=me;PWD=pass')

# Using a DSN, but providing a password as well
# Используя DSN, но также предоставляя пароль
cnxn = pyodbc.connect('DSN=test;PWD=password')

# Create a cursor from the connection
# Создать курсор из соединения
cursor = cnxn.cursor()

'''
en: Make sure you set the encoding or decoding settings needed for your database and the version 
    of Python you are using:
ru: Убедитесь, что вы установили параметры кодирования или декодирования, необходимые для вашей 
    базы данных и используемой версии Python:
'''

# This is just an example that works for PostgreSQL and MySQL, with Python 2.7.
# Это всего лишь пример, который работает для PostgreSQL и MySQL с Python 2.7.
cnxn.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
cnxn.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
cnxn.setencoding(encoding='utf-8')

##################################################################################################
#
# Selecting Some Data (Выбор некоторых данных)
#
##################################################################################################
#  _______________________________________________________________________________________________
# |
# | Select Basics (Выберите основы)
# |_______________________________________________________________________________________________

'''
en: All SQL statements are executed using the Cursor execute() function. If the statement returns rows, 
    such as a select statement, you can retrieve them using the Cursor fetch functions - fetchone(), 
    fetchall(), fetchmany(). If there are no rows, fetchone() will return None, whereas fetchall() and 
    fetchmany() will both return empty lists.
ru: Все операторы SQL выполняются с помощью функции Cursor execute (). Если инструкция возвращает 
    строки, например, оператор select, вы можете получить их, используя функции извлечения 
    Cursor - fetchone (), fetchall (), fetchmany (). Если строк нет, fetchone () вернет None, 
    тогда как fetchall () и fetchmany () оба вернут пустые списки.
'''
cursor.execute("select user_id, user_name from users")
row = cursor.fetchone()
if row:
    print(row)

# Row objects are similar to tuples, but they also allow access to columns by name:
# Объекты строки похожи на кортежи, но они также позволяют доступ к столбцам по имени:
cursor.execute("select user_id, user_name from users")
row = cursor.fetchone()
print('name:', row[1])  # access by column index (zero-based)
print('name:', row.user_name)  # access by name

# The fetchone() function returns None when all rows have been retrieved.
# Функция fetchone () возвращает None, когда все строки получены.
while True:
    row = cursor.fetchone()
    if not row:
        break
    print('id:', row.user_id)

'''
en: The fetchall() function returns all remaining rows in a list. Bear in mind those rows will all 
    be stored in memory so if there a lot of rows, you may run out of memory. If there are no rows, 
    an empty list is returned.
ru: Функция fetchall () возвращает все оставшиеся строки в списке. Помните, что все эти строки будут 
    храниться в памяти, поэтому, если строк много, у вас может не хватить памяти. Если строк нет, 
    возвращается пустой список.
'''
cursor.execute("select user_id, user_name from users")
rows = cursor.fetchall()
for row in rows:
    print(row.user_id, row.user_name)

# If you are going to process the rows one at a time, you can use the cursor itself as an iterator:
# Если вы собираетесь обрабатывать строки по одной, вы можете использовать сам курсор в качестве
# итератора:
cursor.execute("select user_id, user_name from users"):
for row in cursor:
    print(row.user_id, row.user_name)

# or just:
# или просто:
for row in cursor.execute("select user_id, user_name from users"):
    print(row.user_id, row.user_name)

#  _______________________________________________________________________________________________
# |
# | Parameters (Параметры)
# |_______________________________________________________________________________________________

'''
en: ODBC supports parameters using a question mark as a place holder in the SQL. You provide the 
    values for the question marks by passing them after the SQL:
ru: ODBC поддерживает параметры, используя знак вопроса в качестве заполнителя в SQL. Вы 
    предоставляете значения для вопросительных знаков, передавая их после SQL:
'''
cursor.execute("""
    select user_id, user_name
    from users
    where last_logon < ?
    and bill_overdue = ?
""", datetime.date(2001, 1, 1), 'y')
'''
en: This is safer than putting the values into the string because the parameters are passed to the 
    database separately, protecting against SQL injection attacks. It is also be more efficient if 
    you execute the same SQL repeatedly with different parameters. The SQL will be "prepared" only 
    once. (pyodbc keeps only the last prepared statement, so if you switch between statements, each 
    ill be prepared multiple times.)
ru: Это безопаснее, чем помещать значения в строку, потому что параметры передаются в базу данных 
    отдельно, защищая от атак внедрения SQL. Это также будет более эффективно, если вы выполняете 
    один и тот же SQL несколько раз с разными параметрами. SQL будет «подготовлен» только один раз. 
    (pyodbc сохраняет только последний подготовленный оператор, поэтому если вы переключаетесь между 
    операторами, каждый будет подготовлен несколько раз.)
'''

'''
en: The Python DB API specifies that parameters should be passed as a sequence, so this is also 
    supported by pyodbc:
ru: Python DB API указывает, что параметры должны передаваться в виде последовательности, 
    поэтому это также поддерживается pyodbc:
'''
cursor.execute("""
    select user_id, user_name
    from users
    where last_logon < ?
    and bill_overdue = ?
""", [datetime.date(2001, 1, 1), 'y'])

##################################################################################################
#
# Inserting Data (Вставка данных)
#
##################################################################################################
'''
en: To insert data, pass the insert SQL to Cursor execute(), along with any parameters necessary:
ru: Чтобы вставить данные, передайте SQL вставки в Cursor execute () вместе с любыми необходимыми 
    параметрами:
'''
cursor.execute("insert into products(id, name) values ('pyodbc', 'awesome library')")
cnxn.commit()

# or, parameterized:
# или параметризованный:
cursor.execute("insert into products(id, name) values (?, ?)", 'pyodbc', 'awesome library')
cnxn.commit()
'''
en: Note the calls to cnxn.commit(). You must call commit (or set autocommit to True on the 
    connection) otherwise your changes will be lost!
ru: Обратите внимание на вызовы cnxn.commit (). Вы должны вызвать commit (или установить 
    autocommit на True для соединения), иначе ваши изменения будут потеряны!
'''

##################################################################################################
#
# Updating and Deleting (Обновление и удаление)
#
##################################################################################################
'''
en: Updating and deleting work the same way, pass the SQL to execute. However, you often want to 
    know how many records were affected when updating and deleting, in which case you can use the 
    Cursor rowcount attribute:
ru: Обновляя и удаляя работу одинаково, передайте SQL для выполнения. Однако вы часто хотите узнать, 
    сколько записей было затронуто при обновлении и удалении, и в этом случае вы можете использовать 
    атрибут Cursor rowcount:
'''
cursor.execute("delete from products where id <> ?", 'pyodbc')
print(cursor.rowcount, 'products deleted')
cnxn.commit()

'''
en: Since execute() always returns the cursor, you will sometimes see code like this (notice 
    .rowcount on the end).
ru: Так как execute () всегда возвращает курсор, вы иногда увидите такой код (обратите 
    внимание .rowcount в конце).
'''
deleted = cursor.execute("delete from products where id <> 'pyodbc'").rowcount
cnxn.commit()
'''
en: Note the calls to cnxn.commit(). You must call commit (or set autocommit to True on 
    the connection) otherwise your changes will be lost!
ru: Обратите внимание на вызовы cnxn.commit (). Вы должны вызвать commit (или установить 
    autocommit на True для соединения), иначе ваши изменения будут потеряны!
'''

##################################################################################################
#
# Tips and Tricks (Советы и приемы)
#
##################################################################################################
#  _______________________________________________________________________________________________
# |
# | Quotes (Котировки)
# |_______________________________________________________________________________________________

# Since single quotes are valid in SQL, use double quotes to surround your SQL:
# Поскольку одинарные кавычки допустимы в SQL, используйте двойные кавычки,
# чтобы окружить ваш SQL:
deleted = cursor.execute("delete from products where id <> 'pyodbc'").rowcount
'''
en: It's also worthwhile considering using 'raw' strings for your SQL to avoid any inadvertent 
    escaping (unless you really do want to specify control characters):
ru: Также стоит подумать об использовании «сырых» строк для вашего SQL, чтобы избежать непреднамеренного 
    экранирования (если вы действительно не хотите указывать управляющие символы):
'''

#  _______________________________________________________________________________________________
# |
# | Naming Columns (Именование столбцов)
# |_______________________________________________________________________________________________

'''
en: Some databases (e.g. SQL Server) do not generate column names for calculated fields, e.g. 
    COUNT(*). In that case you can either access the column by its index, or use an alias on the 
    column (i.e. use the "as" keyword).
ru: Некоторые базы данных (например, SQL Server) не генерируют имена столбцов для вычисляемых полей, 
    например, COUNT(*) . В этом случае вы можете получить доступ к столбцу по его индексу или 
    использовать псевдоним для столбца (т. Е. Использовать ключевое слово «as»).
'''
row = cursor.execute("select count(*) as user_count from users").fetchone()
print('%s users' % row.user_count)

#  _______________________________________________________________________________________________
# |
# | Formatting Long SQL Statements (Форматирование длинных операторов SQL)
# |_______________________________________________________________________________________________

'''
en: There are many ways of formatting a Python string that encapsulates a long SQL statement. Using 
    the triple-quote string format is the obvious way of doing this. Doing so does create a string 
    with lots of blank space on the left, but white-space (including tabs and newlines) should be 
    ignored by database SQL engines. If you still want to remove the blank space on the left, you 
    can use the dedent() function in the built-in textwrap module. For example:
ru: Существует много способов форматирования строки Python, которая инкапсулирует длинную инструкцию 
    SQL. Использование формата строки с тройными кавычками является очевидным способом сделать это. 
    При этом создается строка с большим количеством пустого пространства слева, но пробелы (включая 
    табуляции и переводы строк) должны игнорироваться механизмами SQL базы данных. Если вы все еще 
    хотите удалить пустое пространство слева, вы можете использовать dedent() во встроенном модуле 
    textwrap . Например:
'''
import textwrap

...
sql = textwrap.dedent("""
    select p.date_of_birth,
           p.email,
           a.city
    from person as p
    left outer join address as a on a.address_id = p.address_id
    where p.status = 'active'
      and p.name = ?
""")
rows = cursor.execute(sql, 'John Smith').fetchall()

#  _______________________________________________________________________________________________
# |
# | fetchval
# |_______________________________________________________________________________________________

'''
en: If you are selecting a single value you can use the fetchval convenience method. If the 
    statement generates a row, it returns the value of the first column of the first row. If there 
    are no rows, None is returned:
ru: Если вы выбираете одно значение, вы можете использовать удобный метод fetchval . Если инструкция 
    генерирует строку, она возвращает значение первого столбца первой строки. Если строк нет, 
    возвращается None:
'''
maxid = cursor.execute("select max(id) from users").fetchval()

'''
en: Most databases support COALESCE or ISNULL which can be used to convert NULL to a hardcoded value, 
    but note that this will not cause a row to be returned if the SQL returns no rows. That is, 
    COALESCE is great with aggregate functions like max or count, but fetchval is better when 
    attempting to retrieve the value from a particular row:
ru: Большинство баз данных поддерживают COALESCE или ISNULL, которые можно использовать для 
    преобразования NULL в жестко заданное значение, но учтите, что это не приведет к возврату 
    строки, если SQL не возвращает строк. То есть COALESCE отлично подходит для агрегатных функций, 
    таких как max или count, но fetchval лучше при попытке извлечь значение из определенной строки:
'''
cursor.execute("select coalesce(max(id), 0) from users").fetchone()[0]
cursor.execute("select coalesce(count(*), 0) from users").fetchone()[0]

# However, fetchval is a better choice if the statement can return an empty set:
# Тем не менее, fetchval является лучшим выбором, если инструкция может возвращать пустой набор:
# Careful! (Осторожно!)
cursor.execute(
    """
    select create_timestamp
    from photos
    where user_id = 1
    order by create_timestamp desc
    limit 1
    """).fetchone()[0]

# Preferred (Предпочитаемый)
cursor.execute(
    """
    select max(updatetime), 0)
    from photos
    where user = 1
    order by create_timestamp desc
    limit 1
    """).fetchval()

'''
en: The first example will raise an exception if there are no rows for user_id 1. The fetchone() 
    call returns None. Python then attempts to apply [0] to the result (None[0]) which is not valid. 
    The fetchval method was created just for this situation - it will detect the fact that there are 
    no rows and will return None.
ru: Первый пример вызовет исключение, если для user_id нет строк 1. fetchone() возвращает None. 
    Затем Python пытается применить [0] к результату ( None[0] ), что недопустимо. Метод fetchval 
    был создан именно для этой ситуации - он обнаружит тот факт, что строк нет, и вернет None.
'''

##################################################################################################
##################################################################################################
###
#                               Getting started (Начиная)
###
##################################################################################################
##################################################################################################

'''
en: The easiest way to check if one of the Microsoft Access ODBC drivers is available to your 
    Python environment (on Windows) is to do:
ru: Самый простой способ проверить, доступен ли один из драйверов ODBC Microsoft Access для вашей 
    среды Python (в Windows), - это:
'''
import pyodbc

[x for x in pyodbc.drivers() if x.startswith('Microsoft Access Driver')]

# Here is an example of how to open an MS Access database:
# Вот пример того, как открыть базу данных MS Access:
conn_str = (
    r'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};'
    r'DBQ=C:\path\to\mydb.accdb;'
)
cnxn = pyodbc.connect(conn_str)
crsr = cnxn.cursor()
for table_info in crsr.tables(tableType='TABLE'):
    print(table_info.table_name)

##################################################################################################
##################################################################################################
###
#                               pyodbc objects
###
##################################################################################################
##################################################################################################


##################################################################################################
#
# The Module
#
##################################################################################################

# You start, of course, by importing the pyodbc module:
# Конечно, вы начинаете с импорта модуля pyodbc:
import pyodbc

##################################################################################################
#
# Connections (связи)
#
##################################################################################################

'''
en: The Connection object represents a single connection to the database and is obtained from the 
    module's connect() function:
ru: Объект Connection представляет одно соединение с базой данных и получается из функции connect 
    () модуля:
'''
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=mine;UID=me;PWD=pwd')
# There are two primary features of the connection object:
# Существуют две основные функции объекта подключения:
# 1. You use cnxn.cursor() to create a new Cursor.
# 1. Вы используете cnxn.cursor () для создания нового курсора.
# 2. You use cnxn.commit() or cnxn.rollback() to commit or rollback work performed with the Cursor.
# 2. Вы используете cnxn.commit () или cnxn.rollback () для фиксации или отката работы, выполняемой
#    с помощью курсора.


#  _______________________________________________________________________________________________
# |
# | commit (совершить)
# |_______________________________________________________________________________________________

'''
en: Unless you have enabled autocommit (in the pyodbc.connect() function or with 
    Connection.autocommit), all uncommitted work will be discarded when the Connection object is 
    closed. You must call cnxn.commit() or your work will be lost!
ru: Если вы не включили автокоммит (в функции pyodbc.connect () или с помощью Connection.autocommit), 
    вся незавершенная работа будет отброшена, когда объект Connection будет закрыт. Вы должны 
    позвонить cnxn.commit (), иначе ваша работа будет потеряна!
'''
cnxn = pyodbc.connect(...)
# do work here...
cnxn.commit()
cnxn.close()

#  _______________________________________________________________________________________________
# |
# | exception handling (Обработка исключений)
# |_______________________________________________________________________________________________

'''
en: The standard Python implementation uses reference counting for garbage collection, so as long 
    as your connection and cursors are not used outside the current function, they will be closed 
    automatically and immediatley when the function exits. Since connections automatically roll 
    back changes (since the last commit call) when they are closed, you do not need a finally block 
    to clean up errors:
ru: Стандартная реализация Python использует подсчет ссылок для сборки мусора, поэтому, если ваше 
    соединение и курсоры не используются вне текущей функции, они будут закрыты автоматически и 
    сразу после выхода из функции. Поскольку соединения автоматически откатывают изменения (начиная 
    с последнего вызова коммита), когда они закрываются, вам не нужен блок finally для устранения 
    ошибок:
'''
cnxn = pyodbc.connect(...)
cursor = cnxn.cursor()
# do work here
# an exception is raised here
cnxn.commit()
'''
en: In this example, the exception causes the function to exit without reaching the commit call. 
    Therefore all the work performed with the cursor will be rolled back.
ru: В этом примере исключение вызывает выход из функции без достижения вызова commit. Поэтому вся 
    работа, выполненная с курсором, будет отменена.
'''

'''
en: If your code might be ported to PyPy or other Python implementations without reference counting, 
    you can use try or with. Connections used in a with block will commit at the end of the block 
    if no errors are raised and will rollback otherwise:
ru: Если ваш код может быть перенесен в PyPy или другие реализации Python без подсчета ссылок, вы 
    можете использовать try или with . Соединения, используемые в блоке with будут зафиксированы в 
    конце блока, если ошибок не возникнет, и в противном случае произойдет откат:
'''
cnxn = pyodbc.connect(...)
cursor = cnxn.cursor()
with cnxn:
    cursor.execute(...)

##################################################################################################
#
# Cursors (курсоры)
#
##################################################################################################

'''
en: Cursor objects are used to execute SQL statements. ODBC and pyodbc allow multiple cursors per 
    connection, but not all databases support this. You can use SQLGetInfo to determine how many 
    concurrent cursors can be supported: cnxn.getinfo(pyodbc.SQL_MAX_CONCURRENT_ACTIVITIES)
ru: Объекты курсора используются для выполнения операторов SQL. ODBC и pyodbc допускают использование 
    нескольких курсоров на соединение, но не все базы данных поддерживают это. Вы можете использовать 
    SQLGetInfo, чтобы определить, сколько одновременных курсоров может поддерживаться: 
    cnxn.getinfo(pyodbc.SQL_MAX_CONCURRENT_ACTIVITIES)
'''
# The most important features of cursors are:
# Наиболее важные особенности курсоров:
# * the execute() function which executes SQL
# * функция execute (), которая выполняет SQL

# * the description tuple which describes the results of a select statement
# * кортеж description, который описывает результаты оператора select

# * rowcount which is the number of rows selected, updated, or deleted
# * rowcount - количество выбранных, обновленных или удаленных строк

# * the fetch functions for accessing the resulting rows
# * функции выборки для доступа к результирующим строкам

cnxn = pyodbc.connect(...)
cursor = cnxn.cursor()

cursor.execute("""
               select user_id, last_logon
               from users
               where last_logon > ?
               and user_type <> 'admin'
               """, twoweeks)

rows = cursor.fetchall()

for row in rows:
    print('user %s logged on at %s' % (row.user_id, row.last_logon))

##################################################################################################
##################################################################################################
###
#                               pyodbc Module
###
##################################################################################################
##################################################################################################


##################################################################################################
#
# Attributes
#
##################################################################################################


#  _______________________________________________________________________________________________
# |
# | version
# |_______________________________________________________________________________________________

'''
en: The module version string in major.minor.patch format such as "4.0.2".
ru: Строка версии модуля в формате major.minor.patch , например "4.0.2" .
'''

#  _______________________________________________________________________________________________
# |
# | apilevel
# |_______________________________________________________________________________________________

'''
en: The string constant "2.0" indicating this module supports the DB API level 2.0
ru: Строковая константа "2.0" указывающая, что этот модуль поддерживает API БД уровня 2.0
'''

#  _______________________________________________________________________________________________
# |
# | lowercase
# |_______________________________________________________________________________________________

'''
en: A Boolean that controls whether column names in result rows are lowercased. This can be changed 
    any time and affects queries executed after the change. The default is False. This can be 
    useful when database columns have inconsistent capitalization.
ru: Логическое значение, определяющее, будут ли имена столбцов в строках результата в нижнем 
    регистре. Это может быть изменено в любое время и влияет на запросы, выполненные после 
    изменения. Значением по умолчанию является False . Это может быть полезно, когда столбцы базы 
    данных имеют несовместимые заглавные буквы.
'''

#  _______________________________________________________________________________________________
# |
# | native_uuid
# |_______________________________________________________________________________________________

'''
en: A Boolean that determines whether SQL_GUID columns are returned as text (False, the default) 
    or uuid.UUID (True) objects. The default is False for backwards compatibility, but this may 
    change in a future release.
ru: Логическое значение, определяющее, возвращаются ли столбцы SQL_GUID в виде текстовых ( False , 
    по умолчанию) или uuid.UUID (True) объектов. По умолчанию установлено значение False для 
    обратной совместимости, но это может измениться в будущем выпуске.
'''

#  _______________________________________________________________________________________________
# |
# | pooling
# |_______________________________________________________________________________________________

'''
en: A Boolean indicating whether connection pooling is enabled. This is a global (HENV) setting, 
    so it can only be modified before the first connection is made. The default is True, which 
    enables ODBC connection pooling.
ru: Логическое значение, указывающее, включен ли пул соединений. Это глобальная настройка (HENV), 
    поэтому ее можно изменить только до первого подключения. По умолчанию установлено значение 
    True , что включает пул соединений ODBC.
'''

#  _______________________________________________________________________________________________
# |
# | threadsafety
# |_______________________________________________________________________________________________

'''
en: The value 1, indicating that threads may share the module but not connections. Note that 
    connections and cursors may be used by different threads, just not at the same time.
ru: Значение 1 , указывающее, что потоки могут совместно использовать модуль, но не соединения. 
    Обратите внимание, что соединения и курсоры могут использоваться разными потоками, но не 
    одновременно.
'''

#  _______________________________________________________________________________________________
# |
# | paramstyle (соединять)
# |_______________________________________________________________________________________________

'''
en: The string constant "qmark" to indicate parameters are identified using question marks.
ru: Строковая константа «qmark» для обозначения параметров идентифицируется с помощью вопросительных 
    знаков.
'''

#  _______________________________________________________________________________________________
# |
# | connect (соединять)
# |_______________________________________________________________________________________________

# Creates and returns a new connection to the database.
# Создает и возвращает новое соединение с базой данных.
connect(*connectionstring, **kwargs) --> Connection

'''
en: The connection string and keywords are put together to construct an ODBC connection string. 
    Python 2 accepts both ANSI and Unicode strings. (In Python 3 all strings are Unicode.)
ru: Строка подключения и ключевые слова объединяются для создания строки подключения ODBC. 
    Python 2 принимает строки как ANSI, так и Unicode. (В Python 3 все строки являются Unicode.)
'''
# a string
cnxn = pyodbc.connect('driver={SQL Server};server=localhost;database=test;uid=me;pwd=me2')
# keywords
cnxn = pyodbc.connect(driver='{SQL Server}', server='localhost', database='test', uid='me', pwd='me2')
# both
cnxn = pyodbc.connect('driver={SQL Server};server=localhost;database=test', uid='me', pwd='me2'
cnxn = pyodbc.connect('DSN=my_dsn', autocommit=True)

'''
en: The DB API recommends some keywords that are not usually used in ODBC connection strings, 
    so they are converted:
ru: DB API рекомендует некоторые ключевые слова, которые обычно не используются в строках 
    соединения ODBC, поэтому они преобразуются:
'''
# +-----------+---------------+
# | keyword   |	converts to   |
# | host      |	server        |
# | user      |	uid           |
# | password  |	pwd           |

# Some keywords are reserved by pyodbc and are not passed to the odbc driver:
# Некоторые ключевые слова зарезервированы pyodbc и не передаются драйверу odbc:

# +==============+=========================================================================+==========+
# | keyword      |	notes	                                                               |  default |
# +==============+=========================================================================+==========+
# | ansi         |	If True, the driver does not support Unicode and SQLDriverConnectA     |          |
# |              |  should be used.                                                        |	False |
# |              |  Если True, драйвер не поддерживает Unicode, и следует использовать     |	      |
# |              |  SQLDriverConnectA.                                                     |	      |
# +--------------+-------------------------------------------------------------------------+----------+
# | attrs_before |	A dictionary of connection attributes to set before connecting.	       |          |
# |              |  Словарь атрибутов подключения, которые нужно установить перед          |	      |
# |              |  подключением.                                                          |	      |
# +--------------+-------------------------------------------------------------------------+----------+
# | autocommit   |	If False, Connection.commit must be called; otherwise each statement   |          |
# |              |  is automatically commited                                              |	False |
# |              |  Если False, Connection.commit должен быть вызван; в противном случае   |	      |
# |              |  каждое утверждение автоматически фиксируется                           |	      |
# +--------------+-------------------------------------------------------------------------+----------+
# | encoding     |	Optional encoding for the connection string.	                       | utf-16le |
# |              |  Необязательное кодирование для строки подключения.                     |	      |
# +--------------+-------------------------------------------------------------------------+----------+
# | readonly     |	If True, the connection is set to readonly                             |	False |
# |              |	Если True, соединение установлено только для чтения                    |	      |  
# +--------------+-------------------------------------------------------------------------+----------+
# | timeout      |	A timeout for the connection, in seconds. This causes the connection's |          |
# |              |  SQL_ATTR_LOGIN_TIMEOUT to be set before the connection occurs.	       |          |
# |              |  Тайм-аут для соединения в секундах. Это приводит к установке           |          |
# |              |  SQL_ATTR_LOGIN_TIMEOUT соединения до того, как соединение будет        |          |
# |              |  установлено.                                                           |          |
# +--------------+-------------------------------------------------------------------------+----------+

#  _______________________________________________________________________________________________
# |
# | ansi
# |_______________________________________________________________________________________________

'''
en: The ansi keyword should only be used to work around driver bugs. pyodbc will determine if the 
    Unicode connection function (SQLDriverConnectW) exists and always attempt to call it. If the 
    driver returns IM001 indicating it does not support the Unicode version, the ANSI version is 
    tried (SQLDriverConnectA). Any other SQLSTATE is turned into an exception. Setting ansi to 
    true skips the Unicode attempt and only connects using the ANSI version. This is useful for 
    drivers that return the wrong SQLSTATE (or if pyodbc is out of date and should support other 
    SQLSTATEs).
ru: Ключевое слово ansi следует использовать только для устранения ошибок драйверов. pyodbc 
    определит, существует ли функция соединения Unicode (SQLDriverConnectW), и всегда будет 
    пытаться ее вызвать. Если драйвер возвращает IM001, указывая, что он не поддерживает версию 
    Unicode, пробуется версия ANSI (SQLDriverConnectA). Любой другой SQLSTATE превращается в 
    исключение. Установка true для ansi пропускает попытку Unicode и подключается только с 
    использованием версии ANSI. Это полезно для драйверов, которые возвращают неправильный 
    SQLSTATE (или если pyodbc устарел и должен поддерживать другие SQLSTATE).
'''


#  _______________________________________________________________________________________________
# |
# | attrs_before
# |_______________________________________________________________________________________________

'''
en: The attrs_before keyword is an optional dictionary of connection attributes. These will be set 
    on the connection via SQLSetConnectAttr before a connection is made. The dictionary keys must 
    be the integer constant defined by ODBC or the driver. Only integer values are supported at 
    this time. Below is an example that sets the SQL_ATTR_PACKET_SIZE connection attribute to 32K.
ru: attrs_before слово attrs_before является необязательным словарем атрибутов соединения. Они будут 
    установлены для соединения через SQLSetConnectAttr до того, как соединение будет установлено. 
    Ключи словаря должны быть целочисленной константой, определенной ODBC или драйвером. В настоящее 
    время поддерживаются только целочисленные значения. Ниже приведен пример, который устанавливает 
    для атрибута соединения SQL_ATTR_PACKET_SIZE значение 32 КБ.
'''
SQL_ATTR_PACKET_SIZE = 112
cnxn = connect(cstring, attrs_before={SQL_ATTR_PACKET_SIZE: 1024 * 32})

#  _______________________________________________________________________________________________
# |
# | dataSources
# |_______________________________________________________________________________________________

dataSources() -> {DSN: Description}
'''
en: Returns a dictionary mapping available DSNs to their descriptions.
    Note: unixODBC may have a bug that only returns items from the users odbc.ini file without 
    merging the system one.
    On Windows, these will be the ones defined in the ODBC Data Source Administrator. On Unix, 
    these will be the ones defined in the user's odbc.ini file (typically ~/.odbc.ini) and/or the 
    system odbc.ini file (typically /etc/odbc.ini).
ru: Возвращает словарь, сопоставляющий доступные DSN с их описаниями.
    Примечание: в unixODBC может быть ошибка, которая возвращает только элементы из 
    пользовательского файла odbc.ini без слияния системного.
    В Windows это будут те, которые определены в Администраторе источника данных ODBC . В Unix это 
    будут файлы, определенные в файле odbc.ini пользователя (обычно ~ / .odbc.ini) и / или 
    системном файле odbc.ini (обычно /etc/odbc.ini).
'''

#  _______________________________________________________________________________________________
# |
# | drivers
# |_______________________________________________________________________________________________

drivers() -> [DriverName1, DriverName2, ... DriverNameN]

'''
en: Returns a datetime.time object initialized from the given ticks value (number of seconds since 
    the epoch; see the documentation of the standard Python time module for details).
ru: Возвращает список драйверов ODBC, доступных для pyodbc. В Windows этот список будет привязан к 
    «платформе», под которой работает скрипт Python (т. Е. 64-разрядный или 32-разрядный).
'''

#  _______________________________________________________________________________________________
# |
# | TimeFromTicks
# |_______________________________________________________________________________________________

t = pyodbc.TimeFromTicks(tics)
'''
en: Returns a datetime.date object initialized from the given ticks value (number of seconds since 
    the epoch; see the documentation of the standard Python time module for details).
ru: Возвращает объект datetime.time, инициализированный из заданного значения тиков (количество 
    секунд с начала эпохи; подробности см. В документации стандартного временного модуля Python).
'''

#  _______________________________________________________________________________________________
# |
# | DateFromTicks
# |_______________________________________________________________________________________________

d = pyodbc.DateFromTicks(d)
'''
en: Returns a datetime.date object initialized from the given ticks value (number of seconds 
    since the epoch; see the documentation of the standard Python time module for details).
ru: Возвращает объект datetime.date, инициализированный из заданного значения тиков (количество 
    секунд с начала эпохи; подробности см. В документации стандартного временного модуля Python).
'''

#  _______________________________________________________________________________________________
# |
# | TimestampFromTicks
# |_______________________________________________________________________________________________

dt = pyodbc.TimestampFromTicks(d)
'''
en: Returns a datetime.datetime object initialized from the given ticks value (number of seconds 
    since the epoch; see the documentation of the standard Python time module for details).
ru: Возвращает объект datetime.datetime, инициализированный из заданного значения тиков (количество 
    секунд с начала эпохи; подробности см. В документации стандартного временного модуля Python).
'''

#  _______________________________________________________________________________________________
# |
# | setDecimalSeparator
# |_______________________________________________________________________________________________

pyodbc.setDecimalSeparator('.')
'''
en: Sets the decimal separator character used when parsing NUMERIC from the database. The default 
    is to use locale's "decimal_point" value or '.' if that fails. This function can be used to 
    override this.
ru: Устанавливает символ десятичного разделителя, используемый при разборе NUMERIC из базы данных. 
    По умолчанию используется значение "decimal_point" или "." если это не удастся. Эта функция 
    может использоваться, чтобы переопределить это.
'''

#  _______________________________________________________________________________________________
# |
# | getDecimalSeparator
# |_______________________________________________________________________________________________

print(pyodbc.getDecimalSeparator())
'''
en: Returns the decimal separator character used when parsing NUMERIC from the database.
ru: Возвращает символ десятичного разделителя, используемый при разборе NUMERIC из базы данных.
'''



##################################################################################################
##################################################################################################
###
#                               Connection 
###
##################################################################################################
##################################################################################################
'''
en: Connection objects manage connections to the database. Each object manages a single ODBC 
    connection (specifically a single HDBC).
ru: Объекты соединений управляют соединениями с базой данных. Каждый объект управляет одним 
    соединением ODBC (в частности, одним HDBC).
'''
myconnection = pyodbc.connect(myconnectionstring, autocommit=True)

##################################################################################################
#
# Connection Attributes (Атрибуты подключения)
#
##################################################################################################


#  _______________________________________________________________________________________________
# |
# | autocommit
# |_______________________________________________________________________________________________

'''
en: True if the connection is in autocommit mode, False otherwise. As per the Python DB API, the 
    default value is False (even though the ODBC default value is True). This value can be changed 
    on a connection dynamically (e.g., cnxn.autocommit = True), and all subsequent SQL statements 
    will be executed using the new setting.
    Auto-commit needs to be True to allow dropping of Databases.
ru: True, если соединение находится в режиме автоматической фиксации, иначе - False. Согласно 
    API Python DB , значением по умолчанию является False (даже если значением по умолчанию ODBC 
    является True ). Это значение может быть изменено в соединении динамически (например, 
    cnxn.autocommit = True ), и все последующие операторы SQL будут выполняться с использованием 
    нового параметра.
    Для автоматической фиксации должно быть установлено значение True чтобы разрешить удаление баз 
    данных.
'''


#  _______________________________________________________________________________________________
# |
# | searchescape
# |_______________________________________________________________________________________________

'''
en: The ODBC search pattern escape character, as returned by SQLGetInfo(SQL_SEARCH_PATTERN_ESCAPE), 
    used to escape special characters such as '%' and ''. These are driver specific.
ru: Экранирующий символ шаблона поиска ODBC, возвращаемый SQLGetInfo (SQL_SEARCH_PATTERN_ESCAPE), 
    используется для экранирования специальных символов, таких как «%» и «». Это зависит от 
    водителя.
'''
SQLGetInfo(SQL_SEARCH_PATTERN_ESCAPE)

#  _______________________________________________________________________________________________
# |
# | timeout
# |_______________________________________________________________________________________________

'''
en: The timeout value, in seconds, for an individual SQL query. Use zero, the default, to disable. 
    The timeout is applied to all cursors created by the connection, so it cannot be changed for a 
    specific cursor or SQL statement. If a query timeout occurs, the database should raise an 
    OperationalError exception with SQLSTATE HYT00 or HYT01.
    Note, this attribute affects only SQL queries. To set the timeout for the actual connection 
    process, use the timeout keyword of the pyodbc.connect() function.
ru: Значение времени ожидания в секундах для отдельного запроса SQL. Используйте ноль, по умолчанию, 
    чтобы отключить. Время ожидания применяется ко всем курсорам, созданным соединением, поэтому 
    его нельзя изменить для конкретного курсора или оператора SQL. Если происходит тайм-аут запроса, 
    база данных должна вызвать исключение OperationalError с SQLSTATE HYT00 или HYT01. Обратите 
    внимание, что этот атрибут влияет только на запросы SQL. Чтобы установить время ожидания для 
    фактического процесса подключения, используйте ключевое слово timeout функции pyodbc.connect().
'''


##################################################################################################
#
# Functions (Атрибуты подключения)
#
##################################################################################################
#  _______________________________________________________________________________________________
# |
# | cursor()
# |_______________________________________________________________________________________________

'''
en: Returns a new Cursor object using the connection.
ru: Возвращает новый объект Cursor, используя соединение.
'''
mycursor = cnxn.cursor()
'''
en: pyodbc supports multiple cursors per connection but your database may not.
ru: pyodbc поддерживает несколько курсоров на соединение, но ваша база данных может этого не делать.
'''


#  _______________________________________________________________________________________________
# |
# | commit()
# |_______________________________________________________________________________________________

'''
en: Commits all SQL statements executed on the connection since the last commit/rollback.
ru: Фиксирует все операторы SQL, выполненные в соединении с момента последней фиксации / отката.
'''
mycursor = cnxn.cursor()
'''
en: Note, this will commit the SQL statements from ALL the cursors created from this connection.
ru: Обратите внимание, что это зафиксирует операторы SQL из ВСЕХ курсоров, созданных из этого 
    соединения.
'''


#  _______________________________________________________________________________________________
# |
# | rollback()
# |_______________________________________________________________________________________________

'''
en: Rolls back all SQL statements executed on the connection since the last commit/rollback.
ru: Откат всех операторов SQL, выполненных в соединении с момента последней фиксации / отката.
'''
cnxn.rollback()
'''
en: You can call this even if no SQL statements have been executed on the connection, allowing 
    it to be used in finally statements, etc.
ru: Вы можете вызвать это, даже если в соединении не было выполнено ни одного оператора SQL, 
    что позволяет использовать его в операторах finally и т. Д.
'''


#  _______________________________________________________________________________________________
# |
# | close()
# |_______________________________________________________________________________________________

'''
en: Closes the connection. Note, any uncommitted effects of SQL statements on the database from 
    this connection will be rolled back and lost forever!
ru: Закрывает соединение. Обратите внимание, что любое непринятое влияние операторов SQL на базу 
    данных из этого соединения будет отменено и потеряно навсегда!
'''
cnxn.close()
'''
en: Connections are automatically closed when they are deleted (typically when they go out of scope) 
    so you should not normally need to call this, but you can explicitly close the connection if 
    you wish. Trying to use a connection after it has been closed will result in a ProgrammingError 
    exception.
ru: Соединения автоматически закрываются, когда они удаляются (как правило, когда они выходят за 
    пределы области видимости), поэтому вам обычно не нужно вызывать это, но вы можете явно закрыть 
    соединение, если хотите. Попытка использовать соединение после его закрытия приведет к 
    исключению ProgrammingError.
'''


#  _______________________________________________________________________________________________
# |
# | getinfo() (получить данные)
# |_______________________________________________________________________________________________

'''
en: This function is not part of the Python DB API. Returns general information about the driver 
    and data source associated with a connection by calling SQLGetInfo, e.g.:
ru: Эта функция не является частью Python DB API. Возвращает общую информацию о драйвере и 
    источнике данных, связанном с соединением путем вызова SQLGetInfo , например:
'''
data_source_name = cnxn.getinfo(pyodbc.SQL_DATA_SOURCE_NAME)
'''
en: See Microsoft's SQLGetInfo documentation for the types of information available.
ru: См. Документацию Microsoft SQLGetInfo для типов доступной информации.
'''


#  _______________________________________________________________________________________________
# |
# | execute() (выполнить)
# |_______________________________________________________________________________________________

'''
en: This function is not part of the Python DB API. Creates a new Cursor object, calls its execute 
    method, and returns the new cursor.
ru: Эта функция не является частью Python DB API. Создает новый объект Cursor, вызывает его метод 
    execute и возвращает новый курсор.
'''
num_products = cnxn.execute("SELECT COUNT(*) FROM product")
'''
en: See Cursor.execute() for more details. This is a convenience method that is not part of the 
    DB API. Since a new Cursor is allocated by each call, this should not be used if more than 
    one SQL statement needs to be executed on the connection.
ru: Смотрите Cursor.execute () для более подробной информации. Это удобный метод, который не 
    является частью API БД. Поскольку каждый курсор выделяется новым курсором, его не следует 
    использовать, если в соединении необходимо выполнить более одного оператора SQL.
'''


#  _______________________________________________________________________________________________
# |
# | add_output_converter()
# |_______________________________________________________________________________________________

'''
en: Register an output converter function that will be called whenever a value with the given SQL 
    type is read from the database.
ru: Зарегистрируйте функцию выходного преобразователя, которая будет вызываться всякий раз, когда 
    значение с данным типом SQL считывается из базы данных.
'''
add_output_converter(sqltype, func)
'''
en: sqltype: the integer SQL type value to convert, which can be one of the defined standard 
    constants (e.g. pyodbc.SQL_VARCHAR) or a database-specific value (e.g. -151 for the 
    SQL Server 2008 geometry data type).
    func: the converter function which will be called with a single parameter, the value, and 
    should return the converted value. If the value is NULL then the parameter passed to the 
    function will be None, otherwise it will be a <class 'bytes'> object.
ru: sqltype : целочисленное значение типа SQL для преобразования, которое может быть одной из 
    определенных стандартных констант (например, pyodbc.SQL_VARCHAR ) или специфичным для базы 
    данных значением (например, -151 для типа данных геометрии SQL Server 2008).
    func : функция преобразователя, которая будет вызываться с одним параметром, значением, и 
    должна возвращать преобразованное значение. Если значение равно NULL, то параметр, передаваемый 
    в функцию, будет иметь значение None , в противном случае это будет объект <class 'bytes'> .
'''


#  _______________________________________________________________________________________________
# |
# | clear_output_converters()
# |_______________________________________________________________________________________________

'''
en: Removes all output converter functions.
ru: Удаляет все функции выходного преобразователя.
'''


#  _______________________________________________________________________________________________
# |
# | remove_output_converter()
# |_______________________________________________________________________________________________

'''
en: Remove a single output converter function previously registered with add_output_converter. 
    (New in version 4.0.25.)
ru: Удалите функцию преобразования с одним выходом, ранее зарегистрированную в add_output_converter. 
    (Новое в версии 4.0.25.)
'''
remove_output_converter(sqltype)

#  _______________________________________________________________________________________________
# |
# | get_output_converter()
# |_______________________________________________________________________________________________

'''
en: Return a reference to the currently active output converter function previously registered with 
    add_output_converter. (New in version 4.0.26.)
ru: Возвращает ссылку на add_output_converter активную функцию выходного преобразователя, ранее 
    зарегистрированную в add_output_converter . (Новое в версии 4.0.26.)
'''
prev_converter = get_output_converter(sqltype)
add_output_converter(sqltype, new_converter)
#
# do stuff that requires the new converter ...
#
add_output_converter(sqltype, prev_converter)  # restore previous behaviour


#  _______________________________________________________________________________________________
# |
# | setencoding()
# |_______________________________________________________________________________________________

# Python 2
cnxn.setencoding(type, encoding=None, ctype=None)

# Python 3
cnxn.setencoding(encoding=None, ctype=None)

# Sets the text encoding for SQL statements and text parameters.
# Устанавливает кодировку текста для операторов SQL и параметров текста.


# ********** type **********
'''
en: The text type to configure. In Python 2 there are two text types: str and unicode which can be 
    configured indivually. Python 3 only has str (which is Unicode), so the parameter is not needed.
ru: Тип текста для настройки. В Python 2 есть два типа текста: str и unicode которые можно настраивать 
    индивидуально. В Python 3 есть только str (то есть Unicode), поэтому параметр не нужен.
'''


# ********** encoding **********
'''
en: The encoding to use. This must be a valid Python encoding that converts text to bytes (Python 3) 
    or str (Python 2).
ru: Кодировка для использования. Это должна быть действительная кодировка Python, которая преобразует 
    текст в bytes (Python 3) или str (Python 2).
'''


# ********** encoding **********
'''
en: The encoding to use. This must be a valid Python encoding that converts text to bytes (Python 3) 
    or str (Python 2).
ru: Кодировка для использования. Это должна быть действительная кодировка Python, которая преобразует 
    текст в bytes (Python 3) или str (Python 2).
'''


# ********** ctype **********
'''
en: The C data type to use when passing data: pyodbc.SQL_CHAR or pyodbc.SQL_WCHAR. If not provided, 
    SQL_WCHAR is used for "utf-16", "utf-16le", and "utf-16be". SQL_CHAR is used for all other 
    encodings. The defaults are:
ru: Тип данных C, используемый при передаче данных: pyodbc.SQL_CHAR или pyodbc.SQL_WCHAR . Если не 
    указано, SQL_WCHAR используется для «utf-16», «utf-16le» и «utf-16be». SQL_CHAR используется 
    для всех других кодировок. Значения по умолчанию:
'''
# +================+================+================+================+
# | Python version |	type 	    |   encoding     |     ctype      |
# +================+================+================+================+
# | Python 2       |	str 	    |   utf-8        |   SQL_CHAR     |
# +----------------+----------------+----------------+----------------+
# | Python 3       |	unicode     |   utf-16le     |   SQL_WCHAR    |
# +----------------+----------------+----------------+----------------+
# | Python 4       |	unicode     |   utf-16le     |   SQL_WCHAR    |
# +----------------+----------------+----------------+----------------+
'''
en: If your database driver communicates with only UTF-8 (often MySQL and PostgreSQL), 
    try the following:
ru: Если ваш драйвер базы данных взаимодействует только с UTF-8 (часто MySQL и PostgreSQL), 
    попробуйте следующее:
'''
# Python 2
cnxn.setencoding(str, encoding='utf-8')
cnxn.setencoding(unicode, encoding='utf-8')

# Python 3
cnxn.setencoding(encoding='utf-8')
'''
en: In Python 2.7, the value "raw" can be used as special encoding for str objects. This will pass 
    the string object's bytes as-is to the database. This is not recommended as you need to make 
    sure that the internal format matches what the database expects.
ru: В Python 2.7 значение «raw» может использоваться как специальная кодировка для объектов str . 
    Это передаст байты строкового объекта как есть в базу данных. Это не рекомендуется, так как 
    вам нужно убедиться, что внутренний формат соответствует тому, что ожидает база данных.
'''

#  _______________________________________________________________________________________________
# |
# | setdecoding
# |_______________________________________________________________________________________________

# Python 2
cnxn.setdecoding(sqltype, encoding=None, ctype=None, to=None)

# Python 3
cnxn.setdecoding(sqltype, encoding=None, ctype=None)

'''
en: Sets the text decoding used when reading SQL_CHAR and SQL_WCHAR from the database.
ru: Устанавливает декодирование текста, используемое при чтении SQL_CHAR и SQL_WCHAR из базы данных.
'''


# ********** sqltype **********
'''
en: The SQL type being configured: pyodbc.SQL_CHAR or pyodbc.SQL_WCHAR.
    There is a special flag, pyodbc.SQL_WMETADATA, for configuring the decoding of column names 
    from SQLDescribeColW.
ru: Настраиваемый тип SQL: pyodbc.SQL_CHAR или pyodbc.SQL_WCHAR .
    Существует специальный флаг pyodbc.SQL_WMETADATA для настройки декодирования имен столбцов 
    из SQLDescribeColW.
'''


# ********** encoding **********
'''
en: The Python encoding to use when decoding the data.
ru: Кодировка Python для использования при декодировании данных.
'''


# ********** ctype **********
'''
en: The C data type to request from SQLGetData: pyodbc.SQL_CHAR or pyodbc.SQL_WCHAR.
ru: Тип данных C для запроса из SQLGetData: pyodbc.SQL_CHAR или pyodbc.SQL_WCHAR .
'''


# ********** to **********
'''
en: The Python 2 text data type to be returned: str or unicode. If not provided (recommended), 
    whatever type the codec returns is returned. (This parameter is not needed in Python 3 because 
    the only text data type is str.)
    The defaults are:
ru: Тип возвращаемых текстовых данных Python 2: str или unicode . Если не указано (рекомендуется), 
    возвращается любой тип, который возвращает кодек. (Этот параметр не требуется в Python 3, 
    потому что единственным типом текстовых данных является str .) 
    Значения по умолчанию:
'''
# +================+================+================+================+
# | Python version |	type 	    |   encoding     |     ctype      |
# +================+================+================+================+
# | Python 2       |	str 	    |   utf-8        |   SQL_CHAR     |
# +----------------+----------------+----------------+----------------+
# | Python 2       |	unicode     |   utf-16le     |   SQL_WCHAR    |
# +----------------+----------------+----------------+----------------+
# | Python 2       |	unicode     |   utf-16le     | SQL_WMETADATA  |
# +----------------+----------------+----------------+----------------+
# | Python 3       |	unicode     |   utf-16le     |   SQL_WCHAR    |
# +----------------+----------------+----------------+----------------+
# | Python 3       |	unicode     |   utf-16le     | SQL_WMETADATA  |
# +----------------+----------------+----------------+----------------+
'''
en: In Python 2.7, the value "raw" can be used as special encoding for SQL_CHAR values. This will 
    create a str object directly from the bytes from the database with no conversion.string 
    object's bytes as-is to the database. This is not recommended as you need to make sure that 
    the internal format matches what the database sends.
ru: В Python 2.7 значение «raw» может использоваться как специальная кодировка для значений 
    SQL_CHAR . Это создаст объект str непосредственно из байтов из базы данных без байтов объекта 
    преобразования.string как есть в базу данных. Это не рекомендуется, так как вам нужно убедиться, 
    что внутренний формат соответствует отправляемой базе данных.
'''


##################################################################################################
#
# Connection objects and the Python context manager syntax (Объекты соединения и синтаксис 
# менеджера контекста Python)
##################################################################################################
'''
en: The Python context manager syntax can be used with Connection objects, and the following code:
ru: Синтаксис диспетчера контекста Python может использоваться с объектами Connection и следующим 
    кодом:
'''
with pyodbc.connect('mydsn') as cnxn:
    do_stuff
'''
en: is exactly equivalent to:
ru: в точности эквивалентно:
'''
cnxn = pyodbc.connect('mydsn')
do_stuff
if not cnxn.autocommit:
    cnxn.commit()

##################################################################################################
##################################################################################################
###
#                               Cursor
###
##################################################################################################
##################################################################################################


##################################################################################################
#
# Attributes
#
##################################################################################################


#  _______________________________________________________________________________________________
# |
# | description
# |_______________________________________________________________________________________________

'''
en: This read-only attribute is a list of 7-item tuples, one tuple for each column returned by the 
    last SQL select statement. Each tuple contains:
    1) column name (or alias, if specified in the SQL)
    2) type code
    3) display size (pyodbc does not set this value)
    4) internal size (in bytes)
    5) precision
    6) scale
    7) nullable (True/False)
    This attribute will be None for operations that do not return rows or if one of the execute 
    methods has not been called. The 'type code' value is the class type used to create the Python 
    objects when reading rows. For example, a varchar column's type will be str.
ru: Этот атрибут «только для чтения» представляет собой список кортежей из 7 элементов, по одному 
    кортежу для каждого столбца, возвращаемого последним оператором выбора SQL. Каждый кортеж 
    содержит:
    1) имя столбца (или псевдоним, если указан в SQL)
    2) код типа
    3) размер дисплея (pyodbc не устанавливает это значение)
    4) внутренний размер (в байтах)
    5) точность
    5) масштаб
    6) обнуляемый (True / False)
    Этот атрибут будет None для операций, которые не возвращают строки или если один из методов 
    execute не был вызван. Значение 'type code' - это тип класса, используемый для создания 
    объектов Python при чтении строк. Например, тип столбца varchar будет str.
'''


#  _______________________________________________________________________________________________
# |
# | rowcount
# |_______________________________________________________________________________________________

'''
en: The number of rows modified by the last SQL statement.
    This is -1 if no SQL has been executed or if the number of rows is unknown. Note that it is 
    not uncommon for databases to report -1 immediately after a SQL select statement for 
    performance reasons. (The exact number may not be known before the first records are returned 
    to the application.)
ru: Количество строк, измененных последним оператором SQL.
    Это -1, если SQL не был выполнен или число строк неизвестно. Обратите внимание, что для 
    повышения производительности базы данных нередко сообщают -1 сразу после оператора выбора SQL. 
    (Точное число может быть неизвестно до того, как первые записи будут возвращены в приложение.)
'''


##################################################################################################
#
# Methods
#
##################################################################################################

#  _______________________________________________________________________________________________
# |
# | execute(sql, *parameters)
# |_______________________________________________________________________________________________

'''
en: Prepares and executes a SQL statement, returning the Cursor object itself. The optional parameters 
    may be passed as a sequence, as specified by the DB API, or as individual values.
ru: Подготавливает и выполняет инструкцию SQL, возвращая сам объект Cursor. Необязательные 
    параметры могут быть переданы в виде последовательности, как указано в API БД, или как 
    отдельные значения.
'''

# standard
cursor.execute("select a from tbl where b=? and c=?", (x, y))
# pyodbc extension
cursor.execute("select a from tbl where b=? and c=?", x, y)

# The return value is always the cursor itself:
# Возвращаемым значением всегда является сам курсор:
for row in cursor.execute("select user_id, user_name from users"):
    print(row.user_id, row.user_name)

row = cursor.execute("select * from tmp").fetchone()
rows = cursor.execute("select * from tmp").fetchall()

count = cursor.execute("update users set last_logon=? where user_id=?", now, user_id).rowcount
count = cursor.execute("delete from users where user_id=1").rowcount
'''
en: As suggested in the DB API, the last prepared statement is kept and reused if you execute 
    the same SQL again, making executing the same SQL with different parameters will be more 
    efficient.
ru: Как предлагается в API БД, последний подготовленный оператор сохраняется и используется 
    повторно, если вы снова выполняете тот же SQL, что делает выполнение того же SQL с другими 
    параметрами более эффективным.
'''


#  _______________________________________________________________________________________________
# |
# | executemany(sql, *params), with fast_executemany=False (the default)
# |_______________________________________________________________________________________________

'''
en: Executes the same SQL statement for each set of parameters, returning None. The single params 
    parameter must be a sequence of sequences, or a generator of sequences.
ru: Выполняет один и тот же оператор SQL для каждого набора параметров, возвращая None. Один 
    параметр params должен быть последовательностью последовательностей или генератором 
    последовательностей.
'''
params = [('A', 1), ('B', 2)]
cursor.executemany("insert into t(name, id) values (?, ?)", params)
'''
en: This will execute the SQL statement twice, once with ('A', 1) and once with ('B', 2). That is, 
    the above code is essentially equivalent to:
ru: Это выполнит оператор SQL дважды, один раз с ('A', 1) и один раз с ('B', 2). То есть приведенный 
    выше код по существу эквивалентен:
'''
params = [('A', 1), ('B', 2)]
for p in params:
    cursor.execute("insert into t(name, id) values (?, ?)", p)
'''
en: Hence, running executemany() with fast_executemany=False is generally not going to be much 
    faster than running multiple execute() commands directly.
    Note, after running executemany(), the number of affected rows is NOT available in the rowcount 
    attribute.
    Also, be careful if autocommit is True. In this scenario, the provided SQL statement will be 
    committed for each and every record in the parameter sequence. So if an error occurs part-way 
    through processing, you will end up with some of the records committed in the database and the 
    rest not, and it may be not be easy to tell which records have been committed. Hence, you may 
    want to consider setting autocommit to False (and explicitly commit() / rollback()) to make 
    sure either all the records are committed to the database or none are, e.g.:
ru: Следовательно, выполнение executemany () с fast_executemany = False обычно не будет намного 
    быстрее, чем непосредственный запуск нескольких команд execute ().
    Обратите внимание, что после запуска executemany () количество затронутых строк НЕ доступно в 
    атрибуте rowcount .
    Также будьте осторожны, если autocommit имеет значение True. В этом случае предоставленный 
    оператор SQL будет зафиксирован для каждой записи в последовательности параметров. Таким 
    образом, если ошибка происходит в процессе обработки, в результате вы получите некоторые 
    записи, зафиксированные в базе данных, а остальные нет, и может быть нелегко определить, какие 
    записи были зафиксированы. Следовательно, вы можете захотеть установить для autocommit значение 
    False (и явно commit() / rollback() ), чтобы убедиться, что либо все записи зафиксированы в базе 
    данных, либо нет, например:
'''
try:
    cnxn.autocommit = False
    params = [('A', 1), ('B', 2)]
    cursor.executemany("insert into t(name, id) values (?, ?)", params)
except pyodbc.DatabaseError as err:
    cnxn.rollback()
else:
    cnxn.commit()
finally:
    cnxn.autocommit = True

#  _______________________________________________________________________________________________
# |
# | executemany(sql, *params), with fast_executemany=True
# |_______________________________________________________________________________________________

'''
en: Executes the SQL statement for the entire set of parameters, returning None. The single params 
    parameter must be a sequence of sequences, or a generator of sequences.
ru: Выполняет инструкцию SQL для всего набора параметров, возвращая None. Один параметр params должен 
    быть последовательностью последовательностей или генератором последовательностей.
'''
params = [('A', 1), ('B', 2)]
cursor.fast_executemany = True
cursor.executemany("insert into t(name, id) values (?, ?)", params)
'''
en: Here, all the parameters are sent to the database server in one bundle (along with the SQL 
    statement), and the database executes the SQL against all the parameters as one database 
    transaction. Hence, this form of executemany() should be much faster than the default 
    executemany(). However, there are limitations to it, see fast_executemany for more details.
    Note, after running executemany(), the number of affected rows is NOT available in the rowcount 
    attribute. 
    Under the hood, there is one important difference when fast_executemany=True. In that case, 
    on the client side, pyodbc converts the Python parameter values to their ODBC "C" equivalents, 
    based on the target column types in the database. E.g., a string-based date parameter value of 
    "2018-07-04" is converted to a C date type binary value by pyodbc before sending it to the 
    database. When fast_executemany=False, that date string is sent as-is to the database and the 
    database does the conversion. This can lead to some subtle differences in behavior depending 
    on whether fast_executemany is True or False.
ru: Здесь все параметры отправляются на сервер базы данных в одном пакете (вместе с оператором SQL), 
    и база данных выполняет SQL для всех параметров как одна транзакция базы данных. Следовательно, 
    эта форма executemany () должна быть намного быстрее, чем по умолчанию executemany (). Тем не 
    менее, есть ограничения, см. fast_executemany для более подробной информации.
    Обратите внимание, что после запуска executemany () количество затронутых строк НЕ доступно в 
    атрибуте rowcount .
    Под капотом есть одно важное отличие, когда fast_executemany = True. В этом случае на стороне 
    клиента pyodbc преобразует значения параметров Python в их эквиваленты ODBC «C» на основе 
    типов целевых столбцов в базе данных. Например, значение параметра даты на основе строки 
    «2018-07-04» преобразуется в двоичное значение типа даты C с помощью pyodbc перед отправкой 
    его в базу данных. Когда fast_executemany = False, эта строка даты отправляется в базу данных 
    как есть, и база данных выполняет преобразование. Это может привести к некоторым тонким 
    различиям в поведении в зависимости от того, является ли fast_executemany истинным или ложным.
'''

#  _______________________________________________________________________________________________
# |
# | fetchone()
# |_______________________________________________________________________________________________

'''
en: Returns the next row in the query, or None when no more data is available.
    A ProgrammingError exception is raised if no SQL has been executed or if it did not return a 
    result set (e.g. was not a SELECT statement).
ru: Возвращает следующую строку в запросе или None, если больше нет доступных данных. Исключение 
    ProgrammingError возникает, если SQL не был выполнен или если он не возвращал набор 
    результатов (например, не был оператором SELECT).
'''
cursor.execute("select user_name from users where user_id=?", userid)
row = cursor.fetchone()
if row:
    print(row.user_name)

#  _______________________________________________________________________________________________
# |
# | fetchval()
# |_______________________________________________________________________________________________

'''
en: Returns the first column of the first row if there are results. For more info see Features 
    beyond the DB API
ru: Возвращает первый столбец первой строки, если есть результаты. Для получения дополнительной 
    информации см. Возможности помимо БД API
'''

#  _______________________________________________________________________________________________
# |
# | fetchall()
# |_______________________________________________________________________________________________

'''
en: Returns a list of all the remaining rows in the query.
    Since this reads all rows into memory, it should not be used if there are a lot of rows. 
    Consider iterating over the rows instead. However, it is useful for freeing up a Cursor so you 
    can perform a second query before processing the resulting rows.
    A ProgrammingError exception is raised if no SQL has been executed or if it did not return a 
    result set (e.g. was not a SELECT statement).
ru: Возвращает список всех оставшихся строк в запросе.
    Поскольку при этом все строки считываются в память, их не следует использовать, если строк 
    много. Попробуйте вместо этого перебирать строки. Однако это полезно для освобождения курсора, 
    чтобы вы могли выполнить второй запрос перед обработкой результирующих строк.
    Исключение ProgrammingError возникает, если SQL не был выполнен или если он не возвращал набор 
    результатов (например, не был оператором SELECT).
'''
cursor.execute("select user_id, user_name from users where user_id < 100")
rows = cursor.fetchall()
for row in rows:
    print(row.user_id, row.user_name)

#  _______________________________________________________________________________________________
# |
# | fetchmany(size=cursor.arraysize)
# |_______________________________________________________________________________________________

'''
en: Returns a list of remaining rows, containing no more than size rows, used to process results 
    in chunks. The list will be empty when there are no more rows.
    The default for cursor.arraysize is 1 which is no different than calling fetchone(). 
    A ProgrammingError exception is raised if no SQL has been executed or if it did not return a 
    result set (e.g. was not a SELECT statement).
ru: Возвращает список оставшихся строк, содержащих не более строк size , используемых для обработки 
    результатов в чанках. Список будет пуст, когда больше нет строк.
    Значение по умолчанию для cursor.arraysize равно 1, что не отличается от вызова fetchone ().
    Исключение ProgrammingError возникает, если SQL не был выполнен или если он не возвращал набор 
    результатов (например, не был оператором SELECT).
'''

#  _______________________________________________________________________________________________
# |
# | commit()
# |_______________________________________________________________________________________________

'''
en: Commits all SQL statements executed on the connection that created this cursor, since the 
    last commit/rollback.
    This affects all cursors created by the same connection! 
    This is no different than calling commit on the connection. The benefit is that many uses can 
    now just use the cursor and not have to track the connection.
ru: Передает все операторы SQL, выполненные для соединения, которое создало этот курсор, начиная с 
    последнего коммита / отката. Это влияет на все курсоры, созданные одним и тем же соединением!
    Это ничем не отличается от вызова commit для соединения. Преимущество заключается в том, что 
    многие пользователи теперь могут просто использовать курсор и не должны отслеживать соединение.
'''

#  _______________________________________________________________________________________________
# |
# | rollback()
# |_______________________________________________________________________________________________

'''
en: Rolls back all SQL statements executed on the connection that created this cursor, since the 
    last commit/rollback.
    This affects all cursors created by the same connection!
ru: Откатывает все операторы SQL, выполненные в соединении, которое создало этот курсор, начиная с 
    последнего коммита / отката.
    Это влияет на все курсоры, созданные одним и тем же соединением!
'''

#  _______________________________________________________________________________________________
# |
# | skip(count)
# |_______________________________________________________________________________________________

'''
en: Skips the next count records in the query by calling SQLFetchScroll with SQL_FETCH_NEXT.
    For convenience, skip(0) is accepted and will do nothing.
ru: Пропускает следующие записи count в запросе, вызывая SQLFetchScroll с SQL_FETCH_NEXT.
    Для удобства skip (0) принимается и ничего не будет делать.
'''

#  _______________________________________________________________________________________________
# |
# | nextset()
# |_______________________________________________________________________________________________

'''
en: This method will make the cursor skip to the next available result set, discarding any 
    remaining rows from the current result set. If there are no more result sets, the method 
    returns False. Otherwise, it returns a True and subsequent calls to the fetch methods will 
    return rows from the next result set.
    This method is primarily used if you have stored procedures that return multiple results.
ru: Этот метод заставит курсор перейти к следующему доступному набору результатов, отбрасывая все 
    оставшиеся строки из текущего набора результатов. Если наборов результатов больше нет, метод 
    возвращает False. В противном случае он возвращает значение True, и последующие вызовы методов 
    выборки будут возвращать строки из следующего набора результатов.
    Этот метод в основном используется, если у вас есть хранимые процедуры, которые возвращают 
    несколько результатов.
'''

#  _______________________________________________________________________________________________
# |
# | close()
# |_______________________________________________________________________________________________

'''
en: Closes the cursor. A ProgrammingError exception will be raised if any operation is attempted 
    with the cursor.
    Cursors are closed automatically when they are deleted (typically when they go out of scope), 
    so calling this is not usually necessary.
ru: Закрывает курсор. Исключение ProgrammingError будет вызвано, если с курсором будет предпринята 
    какая-либо операция.
    Курсоры закрываются автоматически, когда они удаляются (обычно, когда они выходят за пределы 
    области видимости), поэтому вызывать это обычно не нужно.
'''

#  _______________________________________________________________________________________________
# |
# | setinputsizes(list_of_value_tuples)
# |_______________________________________________________________________________________________

'''
en: This optional method can be used to explicitly declare the types and sizes of query parameters. 
    For example:
ru: Этот необязательный метод может использоваться для явного объявления типов и размеров параметров 
    запроса. Например:
'''
sql = "INSERT INTO product (item, price) VALUES (?, ?)"
params = [('bicycle', 499.99), ('ham', 17.95)]
# specify that parameters are for NVARCHAR(50) and DECIMAL(18,4) columns
crsr.setinputsizes([(pyodbc.SQL_WVARCHAR, 50, 0), (pyodbc.SQL_DECIMAL, 18, 4)])
#
crsr.executemany(sql, params)

#  _______________________________________________________________________________________________
# |
# | setoutputsize()
# |_______________________________________________________________________________________________

# This is optional in the API and is not supported.
# Это необязательно в API и не поддерживается.


#  _______________________________________________________________________________________________
# |
# | callproc(procname [,parameters])
# |_______________________________________________________________________________________________

'''
en: This is not yet supported since there is no way for pyodbc to determine which parameters are 
    input, output, or both.
    You will need to call stored procedures using execute(). You can use your database's format 
    or the ODBC escape format. For more information, see the Calling Stored Procedures page.
ru: Это еще не поддерживается, так как pyodbc не может определить, какие параметры являются 
    входными, выходными или обоими.
    Вам нужно будет вызывать хранимые процедуры, используя execute (). Вы можете использовать 
    формат вашей базы данных или escape-формат ODBC. Для получения дополнительной информации см. 
    Страницу « Вызов хранимых процедур» .
'''

#  _______________________________________________________________________________________________
# |
# | tables(table=None, catalog=None, schema=None, tableType=None)
# |_______________________________________________________________________________________________

'''
en: Returns an iterator for generating information about the tables in the database that match the 
    given criteria.
    The table, catalog, and schema interpret the '_' and '%' characters as wildcards. The escape 
    character is driver specific, so use Connection.searchescape.
    Each row has the following columns. See the SQLTables documentation for more information.
    1. table_cat: The catalog name.
    2. table_schem: The schema name.
    3. table_name: The table name.
    4. table_type: One of the string values 'TABLE', 'VIEW', 'SYSTEM TABLE', 'GLOBAL TEMPORARY', 'LOCAL TEMPORARY', 'ALIAS', 'SYNONYM', or a datasource specific type name.
    5. remarks: A description of the table.
ru: Возвращает итератор для генерации информации о таблицах в базе данных, которые соответствуют 
    заданным критериям. 
    Таблица, каталог и схема интерпретируют символы «_» и «%» как символы подстановки. Экранирующий 
    символ зависит от драйвера, поэтому используйте Connection.searchescape.
    Каждая строка имеет следующие столбцы. См. Документацию SQLTables для получения дополнительной 
    информации.
    1. table_cat: имя каталога.
    2. table_schem: имя схемы.
    3. table_name: имя таблицы.
    4. table_type: одно из строковых значений 'TABLE', 'VIEW', 'SYSTEM TABLE', 'GLOBAL TEMPORARY', 
       'LOCAL TEMPORARY', 'ALIAS', 'SYNONYM' или имя типа, специфичного для источника данных.
    5. Примечания: описание таблицы.
'''
for row in cursor.tables():
    print(row.table_name)

# Does table 'x' exist?
if cursor.tables(table='x').fetchone():
    print('yes it does')

#  _______________________________________________________________________________________________
# |
# | columns(table=None, catalog=None, schema=None, column=None)
# |_______________________________________________________________________________________________

'''
en: Creates a result set of column information in the specified tables using the SQLColumns 
    function.
    Each row has the following columns:
    1. table_cat
    2. table_schem
    3. table_name
    4. column_name
    5. data_type
    6. type_name
    7. column_size
    8. buffer_length
    9. decimal_digits
    10. num_prec_radix
    11. nullable
    12. remarks
    13. column_def
    14. sql_data_type
    15. sql_datetime_sub
    16. char_octet_length
    17. ordinal_position
    18. is_nullable: One of SQL_NULLABLE, SQL_NO_NULLS, SQL_NULLS_UNKNOWN.
ru: Создает результирующий набор информации о столбцах в указанных таблицах с помощью функции 
    SQLColumns .
    Каждая строка имеет следующие столбцы:
    1. table_cat
    2. table_schem
    3. table_name
    4. column_name
    5. тип данных
    6. type_name
    7. COLUMN_SIZE
    8. buffer_length
    9. decimal_digits
    10. NUM_PREC_RADIX
    11. обнуляемым
    12. замечания
    13. column_def
    14. sql_data_type
    15. sql_datetime_sub
    16. char_octet_length
    17. ORDINAL_POSITION
    18. is_nullable: один из SQL_NULLABLE, SQL_NO_NULLS, SQL_NULLS_UNKNOWN.
'''
# columns in table x
for row in cursor.columns(table='x'):
    print(row.column_name)

##################################################################################################
##################################################################################################
###
#                               Row
###
##################################################################################################
##################################################################################################

'''
en: Row objects are returned from Cursor fetch functions. As specified in the DB API, they are 
    tuple-like.
ru: Объекты строки возвращаются из функций выборки курсора. Как указано в API БД, они похожи 
    на кортежи.
'''
row = cursor.fetchone()
print(row[0])
'''
en: However, there are some pyodbc additions that make them very convenient:
    * Values can be accessed by column name.
    * The Cursor.description values can be accessed even after the cursor is closed.
    * Values can be replaced.
    * Rows from the same select statement share memory.
    Accessing rows by column name is very convenient, readable, and Pythonish:
ru: Тем не менее, есть некоторые дополнения pyodbc, которые делают их очень удобными:
    * Значения могут быть доступны по имени столбца.
    * Доступ к значениям Cursor.description можно получить даже после закрытия курсора.
    * Значения могут быть заменены.
    * Строки из того же оператора выбора разделяют память.
    Доступ к строкам по имени столбца очень удобен, удобен для чтения и Pythonish:
'''
cursor.execute("select album_id, photo_id from photos where user_id=1")
row = cursor.fetchone()
print(row.album_id, row.photo_id)
print(row[0], row[1])  # same as above, but less readable
'''
en: This will not work if the column name is an invalid Python label (e.g. contains a space or is 
    a Python reserved word), but you can still access by name using row.__getattribute__('My Value').
    Having access to the cursor's description even after the cursor is closed makes Rows very 
    convenient data structures - you can pass them around and they are self describing:
ru: Это не будет работать, если имя столбца является недопустимой меткой Python (например, 
    содержит пробел или является зарезервированным словом Python), но вы все равно можете получить 
    доступ по имени, используя row.__getattribute__('My Value') .
    Доступ к описанию курсора даже после закрытия курсора делает строки очень удобными структурами 
    данных - вы можете передавать их по кругу, и они самоописывают:
'''


def getuser(userid):
    cnxn = pyodbc.connect(...)
    cursor = cnxn.cursor()
    return cursor.execute("""
                          select album_id, photo_id 
                            from photos
                           where user_id = ?
                          """, userid).fetchall()


row = getuser(7)
# At this point the cursor has been closed and deleted
# But the columns and datatypes can still be access:
print('columns:', ', '.join(t[0] for t in row.cursor_description))
'''
en: Unlike normal tuples, values in Row objects can be replaced. (This means you shouldn't use 
    rows as dictionary keys!)
    The intention is to make Rows convenient data structures to replace small or short-lived 
    classes. While SQL is powerful, there are sometimes slight changes that need to be made after 
    reading values:
ru: В отличие от обычных кортежей, значения в объектах Row могут быть заменены. (Это означает, 
    что вы не должны использовать строки в качестве словарных ключей!)
    Намерение состоит в том, чтобы сделать строки удобными структурами данных для замены небольших 
    или недолговечных классов. Несмотря на то, что SQL является мощным, иногда есть небольшие 
    изменения, которые необходимо внести после чтения значений:
'''
# Replace the 'start_date' datetime in each row with one that has a time zone.
rows = cursor.fetchall()
for row in rows:
    row.start_date = row.start_date.astimezone(tz)
'''
en: Note that slicing rows returns tuples, not Row objects!
ru: Обратите внимание, что нарезка строк возвращает кортежи, а не объекты Row!
'''

##################################################################################################
#
# Attributes
#
##################################################################################################


#  _______________________________________________________________________________________________
# |
# | cursor_description
# |_______________________________________________________________________________________________

'''
en: A copy of the Cursor.description object from the Cursor that created this row. This contains 
    the column names and data types of the columns. See Cursor.description
ru: Копия объекта Cursor.description из Cursor, который создал эту строку. Он содержит имена 
    столбцов и типы данных столбцов. Смотрите Cursor.description
'''

##################################################################################################
##################################################################################################
###
#                               Exceptions
###
##################################################################################################
##################################################################################################

'''
en: Python exceptions are raised by pyodbc when ODBC errors are detected. The exception classes 
    specified in the Python DB API specification are used:
    * Error
        * DatabaseError
            * DataError
            * OperationalError
            * IntegrityError
            * InternalError
            * ProgrammingError
            * NotSupportedError
    When an error occurs, the type of exception raised is based on the SQLSTATE value, typically 
    provided by the database.
ru: Python создает исключения Python при обнаружении ошибок ODBC. Используются классы исключений, 
    указанные в спецификации API Python DB :
    * ошибкаОшибка базы данных
        * DataError
            * OperationalError
            * IntegrityError
            * Внутренняя ошибка
            * ProgrammingError
            * NotSupportedError
    При возникновении ошибки тип создаваемого исключения зависит от значения SQLSTATE, обычно 
    предоставляемого базой данных.
'''
# +================+=============================+
# | SQLSTATE       |	Exception 	             |
# +================+=============================+
# | 0A000          |	pyodbc.NotSupportedError |
# +----------------+-----------------------------+
# | 40002          |	pyodbc.IntegrityError    |
# +----------------+-----------------------------+
# | 22***          |	pyodbc.DataError         |
# +----------------+-----------------------------+
# | 23***          |	pyodbc.IntegrityError    |
# +----------------+-----------------------------+
# | 24***          |	pyodbc.ProgrammingError  |
# +----------------+-----------------------------+
# | 25***          |	pyodbc.ProgrammingError  |
# +----------------+-----------------------------+
# | 42***          |	pyodbc.ProgrammingError  |
# +----------------+-----------------------------+
# | HYT00          |	pyodbc.OperationalError  |
# +----------------+-----------------------------+
# | HYT01          |	pyodbc.OperationalError  |
# +----------------+-----------------------------+
'''
en: For example, a primary key error (attempting to insert a value when the key already exists) 
    will raise an IntegrityError.
ru: Например, ошибка первичного ключа (попытка вставить значение, когда ключ уже существует) 
    вызовет ошибку IntegrityError.
'''

##################################################################################################
##################################################################################################
###
#                               Data type conversions from Python to ODBC and back
###                             Преобразование типов данных из Python в ODBC и обратно
##################################################################################################
##################################################################################################


##################################################################################################
#
# Python 3
#
##################################################################################################


#  _______________________________________________________________________________________________
# |
# | Python parameters sent to the database (Параметры Python, отправленные в базу данных)
# |_______________________________________________________________________________________________

'''
en: The following table describes how Python objects passed to Cursor.execute() as parameters are 
    formatted and passed to the driver/database.
ru: В следующей таблице описано, как объекты Python передаются в Cursor.execute () в качестве 
    параметров, которые форматируются и передаются в драйвер / базу данных.
'''
# +=======================+=======================+========================================+
# | Python Datatype       | Description           |	ODBC Datatype 	                       |
# +=======================+=======================+========================================+
# | None                  | varies                | varies (1)                             |
# +-----------------------+-----------------------+----------------------------------------+
# | str                   | UTF-16LE (2)          | SQL_VARCHAR or SQL_LONGVARCHAR (2)(3)  |
# +-----------------------+-----------------------+----------------------------------------+
# | bytes, bytearray      | binary                | SQL_VARBINARY or SQL_LONGVARBINARY (3) |
# +-----------------------+-----------------------+----------------------------------------+
# | bool                  | bit                   |	BIT                                    |
# +-----------------------+-----------------------+----------------------------------------+
# | datetime.date         | date                  |	SQL_TYPE_DATE                          |
# +-----------------------+-----------------------+----------------------------------------+
# | datetime.time         | time                  |	SQL_TYPE_TIME                          |
# +-----------------------+-----------------------+----------------------------------------+
# | datetime.datetime     | timestamp             |	SQL_TIMESTAMP                          |
# +-----------------------+-----------------------+----------------------------------------+
# | int                   | integer               |	SQL_BIGINT                             |
# +-----------------------+-----------------------+----------------------------------------+
# | float                 | floating point        |	SQL_DOUBLE                             |
# +-----------------------+-----------------------+----------------------------------------+
# | decimal               | numeric               |	SQL_NUMERIC                            |
# +-----------------------+-----------------------+----------------------------------------+
# | UUID.uuid             | UUID / GUID           |	SQL_GUID                               |
# +-----------------------+-----------------------+----------------------------------------+
'''
en: 1. If the driver supports it, SQLDescribeParam is used to determine the appropriate type. If 
       not supported, SQL_VARCHAR is used.
    2. The encoding and ODBC data type can be changed using Connect.setdecoding. See the Unicode 
       page.
    3. SQLGetTypeInfo is used to determine when the LONG types are used. If it is not supported, 
       1MB is used.
ru: 1. Если драйвер поддерживает это, SQLDescribeParam используется для определения соответствующего 
       типа. Если не поддерживается, используется SQL_VARCHAR.
    2. Кодировку и тип данных ODBC можно изменить с помощью Connect.setdecoding. Смотрите страницу 
       Unicode .
    3. SQLGetTypeInfo используется, чтобы определить, когда используются типы LONG. Если это не 
       поддерживается, используется 1 МБ.
'''

#  _______________________________________________________________________________________________
# |
# | SQL values received from the database (Значения SQL, полученные из базы данных)
# |_______________________________________________________________________________________________

'''
en: The following table describes how database results are converted to Python objects.
ru: В следующей таблице описано, как результаты базы данных преобразуются в объекты Python.
'''
# +=======================+===========================+========================================+
# | Description           | ODBC Datatype             |	Python Datatype 	                   |
# +=======================+===========================+========================================+
# | NULL                  | any                       | varies (1)                             |
# +-----------------------+---------------------------+----------------------------------------+
# | 1-byte text           | SQL_CHAR                  | SQL_VARCHAR or SQL_LONGVARCHAR (2)(3)  |
# +-----------------------+---------------------------+----------------------------------------+
# | 2-byte text           | SQL_WCHAR                 | SQL_VARBINARY or SQL_LONGVARBINARY (3) |
# +-----------------------+---------------------------+----------------------------------------+
# | UUID / GUID           | SQL_GUID                  |	BIT                                    |
# +-----------------------+---------------------------+----------------------------------------+
# | XML                   | SQL_XML                   |	SQL_TYPE_DATE                          |
# +-----------------------+---------------------------+----------------------------------------+
# | binary                |SQL_BINARY, SQL_VARBINARY  |	SQL_TYPE_TIME                          |
# +-----------------------+---------------------------+----------------------------------------+
# | decimal, numeric      | SQL_DECIMAL               |	SQL_TIMESTAMP                          |
# +-----------------------+---------------------------+----------------------------------------+
# | bit                   | SQL_BIT                   |	SQL_BIGINT                             |
# +-----------------------+---------------------------+----------------------------------------+
# | integers              | SQL_TINYINT, SQL_SMALLINT,|	SQL_DOUBLE                             |
# |                       | SQL_INTEGER, SQL_BIGINT   |	                                       |
# +-----------------------+---------------------------+----------------------------------------+
# | floating point        | SQL_REAL, SQL_FLOAT,      |	SQL_NUMERIC                            |
# |                       | SQL_DOUBLE                |	                                       |
# +-----------------------+---------------------------+----------------------------------------+
# | time                  | SQL_TYPE_TIME             |	SQL_GUID                               |
# +-----------------------+---------------------------+----------------------------------------+
# | SQL Server time       | SS_TIME2                  |	SQL_DOUBLE                             |
# +-----------------------+---------------------------+----------------------------------------+
# | date                  | SQL_TYPE_DATE             |	SQL_NUMERIC                            |
# +-----------------------+---------------------------+----------------------------------------+
# | timestamp             | SQL_TIMESTAMP             |	SQL_GUID                               |
# +-----------------------+---------------------------+----------------------------------------+
'''
en: 1. The encoding can be changed using Connect.setdecoding. See the Unicode page.
    2. The default is str. Setting pyodbc.native_uuid to True will cause them to be returned as 
       UUID.uuid objects..
ru: 1. Кодировка может быть изменена с помощью Connect.setdecoding. Смотрите страницу Unicode .
    2. По умолчанию это str . Если для pyodbc.native_uuid задано значение True, они будут 
       возвращены как объекты UUID.uuid.
'''

##################################################################################################
#
# Python 2
#
##################################################################################################


#  _______________________________________________________________________________________________
# |
# | Python parameters sent to the database (Параметры Python, отправленные в базу данных)
# |_______________________________________________________________________________________________

# +=======================+=======================+========================================+
# | Python Datatype       | Description           |	ODBC Datatype 	                       |
# +=======================+=======================+========================================+
# | None                  | varies                | varies (1)                             |
# +-----------------------+-----------------------+----------------------------------------+
# | str                   | UTF-8 (2)             | SQL_CHAR (2)                           |
# +-----------------------+-----------------------+----------------------------------------+
# | unicode               | UTF-16LE (2)          | SQL_WCHAR (2)                          |
# +-----------------------+-----------------------+----------------------------------------+
# | bytearray             | binary                | SQL_VARBINARY or SQL_LONGVARBINARY (3) |
# +-----------------------+-----------------------+----------------------------------------+
# | buffer                | binary                | SQL_VARBINARY or SQL_LONGVARBINARY (3) |
# +-----------------------+-----------------------+----------------------------------------+
# | bool                  | bit                   |	BIT                                    |
# +-----------------------+-----------------------+----------------------------------------+
# | datetime.date         | date                  |	SQL_TYPE_DATE                          |
# +-----------------------+-----------------------+----------------------------------------+
# | datetime.time         | time                  |	SQL_TYPE_TIME                          |
# +-----------------------+-----------------------+----------------------------------------+
# | datetime.datetime     | timestamp             |	SQL_TIMESTAMP                          |
# +-----------------------+-----------------------+----------------------------------------+
# | int                   | integer               |	SQL_INTEGER                             |
# +-----------------------+-----------------------+----------------------------------------+
# | long                  | bigint                |	SQL_BIGINT                             |
# +-----------------------+-----------------------+----------------------------------------+
# | float                 | floating point        |	SQL_DOUBLE                             |
# +-----------------------+-----------------------+----------------------------------------+
# | decimal               | numeric               |	SQL_NUMERIC                            |
# +-----------------------+-----------------------+----------------------------------------+
# | UUID.uuid             | UUID / GUID           |	SQL_GUID                               |
# +-----------------------+-----------------------+----------------------------------------+
'''
en: 1. If the driver supports it, SQLDescribeParam is used to determine the appropriate type. If 
       not supported, SQL_VARCHAR is used.
    2. The encoding and ODBC data type can be changed using Connect.setdecoding. See the Unicode 
       page.
    3. SQLGetTypeInfo is used to determine when the LONG types are used. If it is not supported, 
       1MB is used.
ru: 1. Если драйвер поддерживает это, SQLDescribeParam используется для определения соответствующего 
       типа. Если не поддерживается, используется SQL_VARCHAR.
    2. Кодировку и тип данных ODBC можно изменить с помощью Connect.setdecoding. Смотрите страницу 
       Unicode .
    3. SQLGetTypeInfo используется, чтобы определить, когда используются типы LONG. Если это не 
       поддерживается, используется 1 МБ.
'''
''

#  _______________________________________________________________________________________________
# |
# | SQL values received from the database (Значения SQL, полученные из базы данных)
# |_______________________________________________________________________________________________

'''
en: The following table describes how database results are converted to Python objects.
ru: В следующей таблице описано, как результаты базы данных преобразуются в объекты Python.
'''
# +=======================+===========================+========================================+
# | Description           | ODBC Datatype             |	Python Datatype 	                   |
# +=======================+===========================+========================================+
# | NULL                  | any                       | None                                   |
# +-----------------------+---------------------------+----------------------------------------+
# | 1-byte text           | SQL_CHAR                  | unicode via UTF-8 (1)                  |
# +-----------------------+---------------------------+----------------------------------------+
# | 2-byte text           | SQL_WCHAR                 | unicode via UTF-16LE (1)               |
# +-----------------------+---------------------------+----------------------------------------+
# | UUID / GUID           | SQL_GUID                  |	unicode or UUID.uuid (2)               |
# +-----------------------+---------------------------+----------------------------------------+
# | XML                   | SQL_XML                   |	unicode                                |
# +-----------------------+---------------------------+----------------------------------------+
# | binary                |SQL_BINARY, SQL_VARBINARY  |	bytearray                              |
# +-----------------------+---------------------------+----------------------------------------+
# | decimal, numeric      | SQL_DECIMAL, SQL_DECIMAL  |	decimal.Decimal                        |
# +-----------------------+---------------------------+----------------------------------------+
# | bit                   | SQL_BIT                   |	bool                                   |
# +-----------------------+---------------------------+----------------------------------------+
# | integers              | SQL_TINYINT, SQL_SMALLINT,|	long                                   |
# |                       | SQL_INTEGER, SQL_BIGINT   |	                                       |
# +-----------------------+---------------------------+----------------------------------------+
# | floating point        | SQL_REAL, SQL_FLOAT,      |	float                                  |
# |                       | SQL_DOUBLE                |	                                       |
# +-----------------------+---------------------------+----------------------------------------+
# | time                  | SQL_TYPE_TIME             |	datetime.time                          |
# +-----------------------+---------------------------+----------------------------------------+
# | SQL Server time       | SS_TIME2                  |	datetime.time                          |
# +-----------------------+---------------------------+----------------------------------------+
# | date                  | SQL_TYPE_DATE             |	datetime.date                          |
# +-----------------------+---------------------------+----------------------------------------+
# | timestamp             | SQL_TIMESTAMP             |	datetime.datetime                      |
# +-----------------------+---------------------------+----------------------------------------+
'''
en: 1. The encoding can be changed using Connect.setdecoding. See the Unicode page.
    2. The default is str. Setting pyodbc.native_uuid to True will cause them to be returned as 
       UUID.uuid objects.
    Note that these are pyodbc 4.x data types. Earlier versions returned str objects for SQL_CHAR 
    buffers and performed no decoding. SQL_WCHAR buffers were assumed to be UCS-2.
ru: 1. Кодировка может быть изменена с помощью Connect.setdecoding. Смотрите страницу Unicode .
    2. По умолчанию это str . Если для pyodbc.native_uuid задано значение True, они будут 
       возвращены как объекты UUID.uuid.
    Обратите внимание, что это типы данных pyodbc 4.x. Более ранние версии возвращали объекты str 
    для буферов SQL_CHAR и не выполняли декодирование. Предполагалось, что буферами SQL_WCHAR 
    являются UCS-2.
'''

##################################################################################################
##################################################################################################
###
#                               Unicode
###
##################################################################################################
##################################################################################################


##################################################################################################
#
# TL; DR
#
##################################################################################################

'''
en: By default, pyodbc uses UTF-16 assuming native byte-order (i.e., UTF-16LE on little-endian 
    machines like x86 and arm) and SQL_C_WCHAR for reading and writing all Unicode as recommended 
    in the ODBC specification. Unfortunately many drivers behave differently so connections may 
    need to be configured. I recommend creating a global connection factory where you can 
    consolidate your connection string and configuration:
ru: По умолчанию pyodbc использует UTF-16, предполагая собственный порядок байтов (т. Е. UTF-16LE 
    на машинах с прямым порядком байтов, таких как x86 и arm) и SQL_C_WCHAR для чтения и записи 
    всего Unicode, как рекомендовано в спецификации ODBC. К сожалению, многие драйверы ведут себя 
    по-разному, поэтому может потребоваться настроить соединения. Я рекомендую создать глобальную 
    фабрику соединений, где вы можете объединить строку подключения и конфигурацию:
'''


def connect():
    cnxn = pyodbc.connect(_connection_string)
    cnxn.setencoding('utf-8')  # (Python 3.x syntax)
    return cnxn


##################################################################################################
#
# Configuring Specific Databases (Конфигурирование определенных баз данных)
#
##################################################################################################
#  _______________________________________________________________________________________________
# |
# | Microsoft SQL Server
# |_______________________________________________________________________________________________

'''
en: SQL Server's recent drivers match the specification, so no configuration is necessary. Using 
    the pyodbc defaults is recommended.
    However, if you want Python 2.7 str results instead of unicode results you may be able to 
    improve performance by setting the encoding to match the database's collation. In particular, 
    it is common to use a latin1 character set and Python has a built-in latin1 codec.
    Check your SQL Server collation using:
ru: Последние драйверы SQL Server соответствуют спецификации, поэтому настройка не требуется. 
    Рекомендуется использовать значения по умолчанию pyodbc.
    Однако, если вам нужны результаты Python 2.7 str вместо результатов в Unicode, вы можете 
    улучшить производительность, установив кодировку в соответствии с сопоставлением базы данных. 
    В частности, обычно используется набор символов latin1, а Python имеет встроенный кодек latin1.
    Проверьте параметры сортировки SQL Server, используя:
'''
select
serverproperty('collation')
'''
en: If it is something like "SQL_Latin1_General_CP1_CI_AS" and you want str results, you may try:
ru: Если это что-то вроде «SQL_Latin1_General_CP1_CI_AS» и вы хотите получить результаты str , 
    вы можете попробовать:
'''
cnxn.setdecoding(pyodbc.SQL_CHAR, encoding='latin1', to=str)
cnxn.setencoding(str, encoding='latin1')
'''
en: It is not recommended, but you can also set the encoding to "raw" which will pass bytes 
    directly between Python str objects and database SQL_C_CHAR buffers. This should only be used 
    if you are certain you know what you are doing as it may not be clear when it doesn't work. 
    It will only work if the database bytes are in the same format is Python's internal format. 
    This is compatible with pyodbc 3.x.
ru: Это не рекомендуется, но вы также можете установить кодировку «raw», которая будет передавать 
    байты непосредственно между объектами Python str и базами данных SQL_C_CHAR. Это следует 
    использовать, только если вы уверены, что знаете, что делаете, так как может быть неясно, 
    когда это не работает. Это будет работать только в том случае, если байты базы данных в том 
    же формате, что и внутренний формат Python. Это совместимо с pyodbc 3.x.
'''
cnxn.setdecoding(pyodbc.SQL_CHAR, encoding='raw')
cnxn.setencoding(str, encoding='raw')

#  _______________________________________________________________________________________________
# |
# | MySQL and PostgreSQL
# |_______________________________________________________________________________________________

'''
en: These databases tend to use a single encoding and do not differentiate between "SQL_CHAR" and 
    "SQL_WCHAR". Therefore when using the Unicode versions of their ODBC drivers you must configure 
    them to encode Unicode data as UTF-8 and to decode both C buffer types using UTF-8.
ru: Эти базы данных обычно используют одну кодировку и не делают различий между «SQL_CHAR» и 
    «SQL_WCHAR». Поэтому при использовании версий Unicode их драйверов ODBC вы должны 
    сконфигурировать их для кодирования данных Unicode как UTF-8 и для декодирования обоих типов 
    буфера C с использованием UTF-8.
'''
# Python 2.7
cnxn.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
cnxn.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
cnxn.setencoding(str, encoding='utf-8')
cnxn.setencoding(unicode, encoding='utf-8')

# Python 3.x
cnxn.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
cnxn.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
cnxn.setencoding(encoding='utf-8')
'''
en: If you are using MySQL you may also need add the character set to the connection string, 
    especially if you will be using supplementary Unicode characters (e.g., emoji):
ru: Если вы используете MySQL, вам также может понадобиться добавить набор символов в строку 
    подключения, особенно если вы будете использовать дополнительные символы Unicode (например, 
    emoji):
'''
# MySQL
cstring = 'DSN=mydsn;charset=utf8mb4'  # or perhaps just utf8 for Unicode characters in the BMP
cnxn = pyodbc.connect(cstring)

#  _______________________________________________________________________________________________
# |
# | Teradata
# |_______________________________________________________________________________________________

'''
en: First, if you are using Teradata on macOS, you are going to have to build pyodbc yourself 
    using iODBC. Hopefully they will compile their driver against unixODBC sometime.
    If you are using UTF-8, most of the lines will be similar to other databases, but unfortunately 
    the metadata on macOS (this may not be necessary on Linux) will return metadata (column names) 
    in UTF-32LE.
ru: Во-первых, если вы используете Teradata в macOS, вам придется самостоятельно собирать pyodbc с 
    использованием iODBC. Надеюсь, они когда-нибудь скомпилируют свой драйвер для unixODBC.
    Если вы используете UTF-8, большинство строк будут похожи на другие базы данных, но, к 
    сожалению, метаданные в macOS (в Linux это может не потребоваться) будут возвращать метаданные 
    (имена столбцов) в UTF-32LE.
'''
# Python 2.7
cnxn.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
cnxn.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
cnxn.setdecoding(pyodbc.SQL_WMETADATA, encoding='utf-32le')
cnxn.setencoding(str, encoding='utf-8')
cnxn.setencoding(unicode, encoding='utf-8')

# Python 3.x
cnxn.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
cnxn.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
cnxn.setdecoding(pyodbc.SQL_WMETADATA, encoding='utf-32le')
cnxn.setencoding(encoding='utf-8')

#  _______________________________________________________________________________________________
# |
# | Exasol
# |_______________________________________________________________________________________________

'''
en: When using Exasol you need to set the encoding for all the SQL_CHAR, SQL_WCHAR, SQL_WMETADATA 
    and Python's strings as UTF-8:
ru: При использовании Exasol вам необходимо установить кодировку для всех SQL_CHAR , SQL_WCHAR , 
    SQL_WMETADATA и Python как UTF-8:
'''
# Python 2.7
cnxn.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
cnxn.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
cnxn.setdecoding(pyodbc.SQL_WMETADATA, encoding='utf-8')
cnxn.setencoding(str, encoding='utf-8')
cnxn.setencoding(unicode, encoding='utf-8')

# Python 3.x
cnxn.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
cnxn.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
cnxn.setdecoding(pyodbc.SQL_WMETADATA, encoding='utf-8')
cnxn.setencoding(encoding='utf-8')

##################################################################################################
#
# Details
#
##################################################################################################
#  _______________________________________________________________________________________________
# |
# | Encodings
# |_______________________________________________________________________________________________

'''
en: The Unicode standard specifies numeric "codes" for each character, such as 65 for 'A' and 229 
    for 'å' (latin small letter a with ring above). However, it does not specify how these numbers 
    should be represented in a computer's memory. The same characters can be represented in 
    multiple ways which are called encodings. For example, here are the example characters above 
    in some different encodings:
ru: Стандарт Unicode определяет числовые «коды» для каждого символа, такие как 65 для «A» и 229 
    для «å» (латинская строчная буква a с кольцом выше). Тем не менее, он не определяет, как эти 
    числа должны быть представлены в памяти компьютера. Одни и те же символы могут быть 
    представлены несколькими способами, которые называются кодировками . Например, вот примеры 
    символов выше в некоторых различных кодировках:
'''
# +=======================+=======================+========================================+
# | Character             | Encoding              |	Bytes 	                               |
# +=======================+=======================+========================================+
# | A                     | latin1                | 0x41                                   |
# +-----------------------+-----------------------+----------------------------------------+
# | A                     | utf-8                 | 0x41                                   |
# +-----------------------+-----------------------+----------------------------------------+
# | A                     | utf-16-le             | 0x4100                                 |
# +-----------------------+-----------------------+----------------------------------------+
# | A                     | utf-16-be             |	0x0041                                 |
# +-----------------------+-----------------------+----------------------------------------+
# | å                     | latin1                |	0xe5                                   |
# +-----------------------+-----------------------+----------------------------------------+
# | å                     | utf-8                 |	0xc3a5                                 |
# +-----------------------+-----------------------+----------------------------------------+
# | å                     | utf-16-le             |	0xe500                                 |
# +-----------------------+-----------------------+----------------------------------------+
# | å                     | utf-16-be             |	0x00e5                                 |
# +-----------------------+-----------------------+----------------------------------------+
'''
en: ASCII characters, such as "A", have values less than 128 and are easy to store in a single 
    byte. Values greater than 127 sometimes are encoded with multiple bytes even if the value 
    itself could fit in a single byte. Notice the UTF-8 encoding of "å" is 0xc3a5 even though 
    its value fits in the single latin1 byte 0xe5.
    IMPORTANT: The thing to note here is that when converting text to bytes, some encoding must 
    be chosen. Even a string as simple as "A" has more than one binary format, as the table above 
    makes clear.
ru: Символы ASCII, такие как «A», имеют значения меньше 128 и их легко хранить в одном байте. 
    Значения, превышающие 127, иногда кодируются несколькими байтами, даже если само значение 
    может помещаться в один байт. Обратите внимание, что кодировка «å» в кодировке UTF-8 равна 
    0xc3a5, хотя ее значение помещается в один байт latin1 0xe5.
    ВАЖНО: здесь следует отметить, что при преобразовании текста в байты необходимо выбирать 
    некоторую кодировку. Даже такая простая строка, как «A», имеет более одного двоичного формата, 
    как показано в таблице выше.
'''

##################################################################################################
#
# ODBC Conversions (ODBC преобразования)
#
##################################################################################################
'''
en: The ODBC specification defines two C data types for character data:
    * SQL_CHAR: A single-byte type like the C char data type, though the sign may differ.
    * SQL_WCHAR: A two-byte data type like a 2-byte wchar_t
    Originally ODBC specified Unicode data to be encoded using UCS-2 and transferred in 
    SQL_WCHAR buffers. Later this was changed to allow UTF-8 in SQL_CHAR buffers and UTF-16LE in 
    SQL_WCHAR buffers.
    By default pyodbc reads all text columns as SQL_C_WCHAR buffers and decodes them using UTF-16LE. 
    When writing, it always encodes using UTF-16LE into SQL_C_WCHAR buffers.
ru: Спецификация ODBC определяет два типа данных C для символьных данных:
    * SQL_CHAR: однобайтовый тип, такой как тип данных C char , хотя знак может отличаться.
    * SQL_WCHAR: двухбайтовый тип данных, например, 2-байтовый wchar_t
    Первоначально ODBC определял данные Unicode для кодирования с использованием UCS-2 и передачи 
    в буферах SQL_WCHAR. Позже это было изменено, чтобы разрешить UTF-8 в буферах SQL_CHAR и 
    UTF-16LE в буферах SQL_WCHAR.
    По умолчанию pyodbc читает все текстовые столбцы как буферы SQL_C_WCHAR и декодирует их, 
    используя UTF-16LE. При записи он всегда кодирует с использованием UTF-16LE в буферы 
    SQL_C_WCHAR.
'''

##################################################################################################
#
# Configuring (Настройка)
#
##################################################################################################

'''
en: If the defaults above do not match your database driver, you can use the Connection.setencoding 
    and Connection.setdecoding functions.
ru: Если приведенные выше значения по умолчанию не соответствуют вашему драйверу базы данных, вы 
    можете использовать функции Connection.setencoding и Connection.setdecoding .
'''

#  _______________________________________________________________________________________________
# |
# | Python 3
# |_______________________________________________________________________________________________

cnxn.setencoding(encoding=None, ctype=None)
'''
en: This sets the encoding used when writing an str object to the database and the C data type. 
    (The data is always written to an array of bytes, but we must tell the database if it should 
    treat the buffer as an array of SQL_CHARs or SQL_WCHARs.)
    The encoding must be a valid Python encoding that converts text to bytes. Optimized code is 
    used for "utf-8", "utf-16", "utf-16le", and "utf-16be".
    The result is always an array of bytes but we must also tell the ODBC driver if it should 
    treat the buffer as an array of SQL_CHAR or SQL_WCHAR elements. If not provided, 
    pyodbc.SQL_WCHAR is used for the UTF-16 variants and pyodbc.SQL_CHAR is used for everything 
    else.
ru: Это устанавливает кодировку, используемую при записи объекта str в базу данных, и тип данных C. 
    (Данные всегда записываются в массив байтов, но мы должны сообщить базе данных, следует ли ей 
    рассматривать буфер как массив SQL_CHAR или SQL_WCHAR.)
    encoding должна быть допустимой кодировкой Python, которая преобразует текст в bytes . 
    Оптимизированный код используется для "utf-8", "utf-16", "utf-16le" и "utf-16be".
    Результатом всегда является массив байтов, но мы также должны сообщить драйверу ODBC, следует 
    ли ему рассматривать буфер как массив элементов SQL_CHAR или SQL_WCHAR. Если не указано, 
    pyodbc.SQL_WCHAR используется для вариантов UTF-16, а pyodbc.SQL_CHAR используется для всего 
    остального.
'''
cnxn.setdecoding(sqltype, encoding=None, ctype=None)
'''
en: This sets the encoding used when reading a buffer from the database and converting it to an 
    str object.
    sqltype controls which SQL type being configured: pyodbc.SQL_CHAR or pyodbc.SQL_WCHAR. Use 
    SQL_CHAR to configure the encoding when reading a SQL_CHAR buffer, etc.
    There is also a special constant, pyodbc.SQL_WMETADATA, for configuring how column names are 
    read. You would expect drivers to return them the same way they return data, but that's rarely 
    the case.
    When a buffer is being read, the driver will tell pyodbc whether it is a SQL_CHAR or SQL_WCHAR 
    buffer. However, pyodbc can request the data be converted to either of these formats. Most of 
    the time this parameter should not be supplied and the same type will be used, but this can be 
    useful in the case where the driver reports SQL_WCHAR data but the data is actually in UTF-8.
ru: Это устанавливает кодировку, используемую при чтении буфера из базы данных и преобразовании 
    его в объект str .
    sqltype контролирует, какой тип SQL настраивается: pyodbc.SQL_CHAR или pyodbc.SQL_WCHAR . 
    Используйте SQL_CHAR для настройки кодировки при чтении буфера SQL_CHAR и т. Д.
    Существует также специальная константа, pyodbc.SQL_WMETADATA , для настройки способа чтения 
    имен столбцов. Можно ожидать, что драйверы будут возвращать их так же, как они возвращают 
    данные, но это редко происходит.
    Когда буфер читается, драйвер сообщит pyodbc, является ли он буфером SQL_CHAR или SQL_WCHAR. 
    Однако pyodbc может запросить преобразование данных в любой из этих форматов. В большинстве 
    случаев этот параметр не следует указывать, и будет использоваться один и тот же тип, но это 
    может быть полезно в том случае, если драйвер сообщает данные SQL_WCHAR, но на самом деле эти 
    данные находятся в UTF-8.
'''

#  _______________________________________________________________________________________________
# |
# | Python 2
# |_______________________________________________________________________________________________
'''
en: The Python 2 version of setencoding starts with a type parameter so that str and unicode can be 
    configured independantly.
ru: Версия setencoding для Python 2 начинается с параметра типа, так что str и unicode можно настраивать 
    независимо.
'''

# cnxn.setencoding(type, encoding=None, ctype=None)

cnxn.setencoding(str, encoding='utf-8')
cnxn.setencoding(unicode, encoding='utf-8', ctype=pyodbc.SQL_CHAR)

##################################################################################################
##################################################################################################
###
#                               Database Transaction Management
###
##################################################################################################
##################################################################################################
'''
en: Those of you who come from a database background will be familiar with the idea of database 
    transactions, i.e. when a series of SQL statements are committed together (or rolled-back) in 
    one operation. Transactions are crucial if you need to make multiple updates to a database 
    where each update would leave the database in an inconsistent state, albeit temporarily. The 
    classic example of this is processing a check, where money is transferred from one bank account 
    to another, i.e. a debit from one account and a credit to another account. It is important that 
    both the debit and credit are committed together otherwise there will be an inconsistency within 
    the database.
    Note, this whole article is relevant only when autocommit is set to False on the pyodbc 
    connection (False is the default). When autocommit is set to True, a commit is automatically 
    executed after every SQL statement (by the database itself, not pyodbc). To put it another way, 
    every SQL statement is essentially run within its own database transaction.
    When using pyodbc with autocommit=False, it is important to understand that you never 
    explicitly open a database transaction in your Python code. Instead, a database transaction is 
    implicitly opened when a Connection object is created with pyodbc.connect(). That database 
    transaction is then either committed or rolled-back by explicitly calling commit() or 
    rollback(), at which point a new database transaction is implicitly opened. SQL statements are 
    executed using the Cursor.execute() function. Hence, the equivalent of the following SQL:
ru: Те из вас, кто пришел из базы данных, будут знакомы с идеей транзакций базы данных, то есть 
    когда ряд операторов SQL фиксируется вместе (или откатывается) в одной операции. Транзакции 
    имеют решающее значение, если вам нужно сделать несколько обновлений базы данных, где каждое 
    обновление оставляло бы базу данных в несовместимом состоянии, хотя и временно. Классическим 
    примером этого является обработка чека, когда деньги переводятся с одного банковского счета на 
    другой, то есть дебет с одного счета и кредит на другой счет. Важно, чтобы как дебет, так и 
    кредит были зафиксированы вместе, иначе в базе данных будет несогласованность.
    Обратите внимание, что вся эта статья актуальна, только если для autocommit установлено 
    значение False для соединения pyodbc (False по умолчанию). Когда для autocommit установлено 
    значение True, фиксация выполняется автоматически после каждого оператора SQL (самой базой 
    данных, а не pyodbc). Иными словами, каждый оператор SQL по сути выполняется внутри своей 
    транзакции базы данных.
    При использовании pyodbc с autocommit autocommit=False важно понимать, что вы никогда не 
    открываете явно транзакцию базы данных в своем коде Python. Вместо этого транзакция базы 
    данных неявно открывается при создании объекта Connection с помощью pyodbc.connect() . Затем 
    эта транзакция базы данных либо фиксируется, либо откатывается с помощью явного вызова commit() 
    или rollback() , после чего новая транзакция базы данных неявно открывается. Операторы SQL 
    выполняются с использованием функции Cursor.execute() . Следовательно, эквивалент следующего SQL:
'''
BEGIN
TRANSACTION
UPDATE
T1
SET...
DELETE
FROM
T1
WHERE...
INSERT
INTO
T1
VALUES...
COMMIT
TRANSACTION
BEGIN
TRANSACTION
INSERT
INTO
T2
VALUES...
INSERT
INTO
T3
VALUES...
COMMIT
TRANSACTION
'''
en: in Python would be:
ru: в Python будет:
'''
cnxn = pyodbc.connect('mydsn', autocommit=False)
crsr = cnxn.cursor()
crsr.execute("UPDATE T1 SET ...")
crsr.execute("DELETE FROM T1 WHERE ...")
crsr.execute("INSERT INTO T1 VALUES ...")
cnxn.commit()
crsr.execute("INSERT INTO T2 VALUES ...")
crsr.execute("INSERT INTO T3 VALUES ...")
cnxn.commit()
cnxn.close()
'''
en: As you can see, no database transaction is ever explicitly opened in Python but they are 
    explicitly committed. Also, it's important to remember that transactions are managed through 
    connections, not cursors. Cursors are merely vehicles to execute SQL statements and manage 
    their results, nothing more. Yes, there is a convenience function commit() on the Cursor 
    object but that simply calls commit() on the cursor's parent Connection object. Bear in mind 
    too that when commit() is called on a connection, ALL the updates from ALL the cursors on that 
    connection are committed together (ditto for rollback()).
    When a connection is closed with the close() function, a rollback is always issued on the 
    connection just in case. When a Connection object goes out of scope before it is closed 
    (e.g. because an exception occurs), the Connection object is automatically deleted by Python, 
    and a rollback is issued as part of the deletion process.
ru: Как видите, ни одна транзакция базы данных никогда не открывается явно в Python, но они явно 
    фиксируются. Также важно помнить, что транзакции управляются через соединения, а не курсоры. 
    Курсоры - это просто средства для выполнения операторов SQL и управления их результатами, не 
    более того. Да, для объекта Cursor есть удобная функция commit() но она просто вызывает commit() 
    для родительского объекта Connection курсора. Помните также, что когда commit() вызывается для 
    соединения, ВСЕ обновления от ВСЕХ курсоров на этом соединении фиксируются вместе (то же самое 
    для rollback() ).
    Когда соединение закрывается с помощью функции close() , откат всегда выдается на соединение 
    на всякий случай. Когда объект Connection выходит из области действия до его закрытия 
    (например, из-за возникновения исключения), объект Connection автоматически удаляется Python, 
    и откат выполняется как часть процесса удаления.
'''

#  _______________________________________________________________________________________________
# |
# | Specifying a Transaction Isolation level (Указание уровня изоляции транзакции)
# |_______________________________________________________________________________________________
'''
en: Database management systems that support transactions often support several levels of 
    transaction isolation to control the effects of multiple processes performing simultaneous 
    operations within their own transactions. ODBC supports four (4) levels of transaction 
    isolation:
    * SQL_TXN_READ_UNCOMMITTED
    * SQL_TXN_READ_COMMITTED
    * SQL_TXN_REPEATABLE_READ
    * SQL_TXN_SERIALIZABLE
    You can specify one of these in your Python code using the Connection#set_attr method, e.g.,
ru: Системы управления базами данных, которые поддерживают транзакции, часто поддерживают несколько 
    уровней изоляции транзакций, чтобы контролировать влияние нескольких процессов, выполняющих 
    одновременные операции в рамках своих собственных транзакций. ODBC поддерживает четыре (4) 
    уровня изоляции транзакций:
    * SQL_TXN_READ_UNCOMMITTED
    * SQL_TXN_READ_COMMITTED
    * SQL_TXN_REPEATABLE_READ
    * SQL_TXN_SERIALIZABLE
    Вы можете указать один из них в своем коде Python, используя метод Connection#set_attr , 
    например,
'''
cnxn = pyodbc.connect(conn_str, autocommit=True)
cnxn.set_attr(pyodbc.SQL_ATTR_TXN_ISOLATION, pyodbc.SQL_TXN_SERIALIZABLE)
cnxn.autocommit = False  # enable transactions
'''
en: Note that a particular database engine may not support all four isolation levels. For example, 
    Microsoft Access only supports SQL_TXN_READ_COMMITTED.
ru: Обратите внимание, что конкретный механизм базы данных может не поддерживать все четыре уровня 
    изоляции. Например, Microsoft Access поддерживает только SQL_TXN_READ_COMMITTED .
'''

##################################################################################################
##################################################################################################
###
#                               Calling Stored Procedures
###                             (Вызов хранимых процедур)
##################################################################################################
##################################################################################################
'''
en: pyodbc does not currently implement the optional .callproc method. (It has been investigated.)
    However, ODBC defines a {CALL ...} escape sequence that should be supported by well-behaved 
    ODBC drivers.
    For example, to call a stored procedure named "usp_NoParameters" that takes no parameters, we 
    can do
ru: В настоящее время pyodbc не реализует необязательный метод .callproc . (Это было исследовано .)
    Однако ODBC определяет escape-последовательность {CALL ...}, которая должна поддерживаться 
    хорошими драйверами ODBC.
    Например, чтобы вызвать хранимую процедуру с именем «usp_NoParameters», которая не принимает 
    параметров, мы можем сделать
'''
crsr.execute("{CALL usp_NoParameters}")
'''
en: To call a stored procedure that takes only input parameters, we can do
ru: Чтобы вызвать хранимую процедуру, которая принимает только входные параметры, мы можем сделать
'''
params = (14, "Dinsdale")
crsr.execute("{CALL usp_UpdateFirstName (?,?)}", params)

#  _______________________________________________________________________________________________
# |
# | Output Parameters and Return Values (Выходные параметры и возвращаемые значения)
# |_______________________________________________________________________________________________
'''
en: Because pyodbc does not have .callproc we need to use a workaround for retrieving the values 
    of output parameters and return values. The specific method will depend on what your particular 
    ODBC driver supports, but for Microsoft's ODBC drivers for SQL Server we can use an "anonymous 
    code block" to EXEC the stored procedure and then SELECT the output parameters and/or return 
    values. For example, for the SQL Server stored procedure
ru: Поскольку pyodbc не имеет .callproc нам нужно использовать обходной путь для получения значений 
    выходных параметров и возвращаемых значений. Конкретный метод будет зависеть от того, что 
    поддерживает ваш конкретный драйвер ODBC, но для драйверов ODBC от Microsoft для SQL Server мы 
    можем использовать «блок анонимного кода», чтобы ВЫПОЛНИТЬ хранимую процедуру, а затем ВЫБРАТЬ 
    выходные параметры и / или возвращаемые значения. Например, для хранимой процедуры SQL Server
'''
CREATE
PROCEDURE[dbo].[test_for_pyodbc]


@param_in


nvarchar(max) = N
'',


@param_out


nvarchar(max)
OUTPUT
AS
BEGIN
SET
NOCOUNT
ON;

-- set
output
parameter
SELECT @ param_out = N
'Output parameter value: You said "' +


@param_in


+ N
'".';

-- also
return a
couple
of
result
sets
SELECT
N
'SP result set 1, row 1'
AS
foo
UNION
ALL
SELECT
N
'SP result set 1, row 2'
AS
foo;

SELECT
N
'SP result set 2, row 1'
AS
bar
UNION
ALL
SELECT
N
'SP result set 2, row 2'
AS
bar;
END
'''
en: our Python code can do this
ru: наш код Python может сделать это
'''
sql = """\
DECLARE @out nvarchar(max);
EXEC [dbo].[test_for_pyodbc] @param_in = ?, @param_out = @out OUTPUT;
SELECT @out AS the_output;
"""
params = ("Burma!",)
crsr.execute(sql, params)
rows = crsr.fetchall()
while rows:
    print(rows)
    if crsr.nextset():
        rows = crsr.fetchall()
    else:
        rows = None
'''
en: to produce this:
ru: произвести это:
'''
[('SP result set 1, row 1',), ('SP result set 1, row 2',)]
[('SP result set 2, row 1',), ('SP result set 2, row 2',)]
[('Output parameter value: You said "Burma!".',)]
'''
en: Notice that the result set(s) created by the stored procedure are returned first, followed by 
    the result set with the output parameter(s) as returned by the SELECT statement in the 
    anonymous code block passed to the pyodbc .execute method.
    Similarly, for a SQL Server stored procedure with a RETURN value we can use something like this:
ru: Обратите внимание, что наборы результатов, созданные хранимой процедурой, возвращаются первыми, 
    а затем набор результатов с выходными параметрами, возвращаемыми оператором SELECT в блоке 
    анонимного кода, передаваемого методу .execute .
    Аналогично, для хранимой процедуры SQL Server со значением RETURN мы можем использовать что-то 
    вроде этого:
'''
sql = """\
DECLARE @rv int;
EXEC @rv = [dbo].[another_test_sp];
SELECT @rv AS return_value;
"""
crsr.execute(sql)
return_value = crsr.fetchval()
''

##################################################################################################
##################################################################################################
###
#                               Features beyond the DB API
###                            (Возможности помимо БД API)
##################################################################################################
##################################################################################################


##################################################################################################
#
# Introduction (Введение)
#
##################################################################################################

'''
en: The following features are beyond the requirements of DB API 2.0. They are intended to provide 
    a very Python-like, convenient programming experience, but you should not use them if your 
    code needs to be portable between DB API modules. (Though I hope future DB API specifications 
    will adopt some of these features.)
ru: Следующие функции выходят за рамки требований DB API 2.0 . Они предназначены для обеспечения 
    очень похожего на Python удобного опыта программирования, но вы не должны использовать их, 
    если ваш код должен быть переносимым между модулями API БД. (Хотя я надеюсь, что будущие 
    спецификации API БД примут некоторые из этих функций.)
'''

##################################################################################################
#
# fetchval
#
##################################################################################################

'''
en: The fetchval() convenience method returns the first column of the first row if there are results, 
    otherwise it returns None.
ru: Удобный метод fetchval() возвращает первый столбец первой строки, если есть результаты, в 
    противном случае он возвращает None.
'''
count = cursor.execute('select count(*) from users').fetchval()

##################################################################################################
#
# fast_executemany
#
##################################################################################################

'''
en: (New in version 4.0.19.) Simply adding
ru: (Новое в версии 4.0.19.) Просто добавление
'''
# crsr is a pyodbc.Cursor object
crsr.fast_executemany = True
'''
en: can boost the performance of executemany operations by greatly reducing the number of 
    round-trips to the server.
    Notes:
    * This feature is "off" by default, and is currently only recommended for applications that 
      use Microsoft's ODBC Driver for SQL Server.
    * The parameter values are held in memory, so very large numbers of records (tens of millions 
      or more) may cause memory issues.
    * Writing fractional seconds of datetime.time values is supported, unlike normal pyodbc 
      behavior
    * See this tip regarding fast_executemany and temporary tables.
    * For information on using fast_executemany with SQLAlchemy (and pandas) see the Stack 
      Overflow question here.
ru: может повысить производительность выполнения executemany операций за счет значительного 
    сокращения числа обращений к серверу.
    Примечания:
    * Эта функция по умолчанию отключена и в настоящее время рекомендуется только для приложений, 
      использующих драйвер ODBC от Microsoft для SQL Server.
    * Значения параметров хранятся в памяти, поэтому очень большое количество записей (десятки 
      миллионов и более) может вызвать проблемы с памятью.
    * Поддерживается запись дробных секунд значений datetime.time , в отличие от обычного поведения 
      pyodbc
    * Смотрите этот совет относительно fast_executemany и временных таблиц.
    * Для получения информации об использовании fast_executemany с SQLAlchemy (и пандами) см. 
      Вопрос переполнения стека здесь .
'''

##################################################################################################
#
# Access Values By Name (Доступ к значениям по имени)
#
##################################################################################################
'''
en: The DB API specifies that results must be tuple-like, so columns are normally accessed by 
    indexing into the sequence (e.g. row[0]) and pyodbc supports this. However, columns can also 
    be accessed by name:
ru: DB API указывает, что результаты должны быть подобны кортежу, поэтому доступ к столбцам обычно 
    осуществляется путем индексации в последовательности (например, row[0] ), и pyodbc поддерживает 
    это. Тем не менее, столбцы также могут быть доступны по имени:
'''
cursor.execute("select album_id, photo_id from photos where user_id=1")
row = cursor.fetchone()
print(row.album_id, row.photo_id)
print(row[0], row[1])  # same as above, but less readable
'''
en: This makes the code easier to maintain when modifying SQL, more readable, and allows rows to 
    be used where a custom class might otherwise be used. All rows from a single execute share 
    the same dictionary of column names, so using Row objects to hold a large result set may also 
    use less memory than creating a object for each row.
    The SQL "as" keyword allows the name of a column in the result set to be specified. This is 
    useful if a column name has spaces or if there is no name:
ru: Это облегчает поддержку кода при изменении SQL, делает его более читаемым и позволяет 
    использовать строки, где в противном случае может использоваться пользовательский класс. Все 
    строки из одного выполнения совместно используют один и тот же словарь имен столбцов, поэтому 
    использование объектов Row для хранения большого результирующего набора может также 
    использовать меньше памяти, чем создание объекта для каждой строки.
    Ключевое слово SQL «as» позволяет указать имя столбца в наборе результатов. Это полезно, если 
    в имени столбца есть пробелы или если имени нет:
'''
cursor.execute("select count(*) as photo_count from photos where user_id < 100")
row = cursor.fetchone()
print(row.photo_count)

##################################################################################################
#
# Rows Values Can Be Replaced (Значения строк могут быть заменены)
#
##################################################################################################
'''
en: Though SQL is very powerful, values sometimes need to be modified before they can be used. 
    Rows allow their values to be replaced, which makes them even more convenient ad-hoc data 
    structures.
ru: Хотя SQL является очень мощным средством, иногда значения необходимо изменить, прежде чем их 
    можно будет использовать. Строки позволяют заменять их значения, что делает их еще более 
    удобными специальными структурами данных.
'''
# Replace the 'start_date' datetime in each row with one that has a time zone.
rows = cursor.fetchall()
for row in rows:
    row.start_date = row.start_date.astimezone(tz)
'''
en: Note that columns cannot be added to rows; only values for existing columns can be modified.
ru: Обратите внимание, что столбцы нельзя добавлять в строки; только значения для существующих столбцов 
    могут быть изменены.
'''

##################################################################################################
#
# Cursors are Iterable (Курсоры повторяемы)
#
##################################################################################################
'''
en: The DB API makes this an optional feature. Each iteration returns a row object.
ru: DB API делает это дополнительной функцией. Каждая итерация возвращает объект строки.
'''
cursor.execute("select album_id, photo_id from photos where user_id=1")
for row in cursor:
    print(row.album_id, row.photo_id)

##################################################################################################
#
# Cursor.execute() Returns the Cursor (Возвращает курсор)
#
##################################################################################################
'''
en: The DB API specification does not specify the return value of Cursor.execute(). Previous 
    versions of pyodbc (2.0.x) returned different values, but the 2.1+ versions always return the 
    Cursor itself.
    This allows for compact code such as:
ru: Спецификация DB API не указывает возвращаемое значение Cursor.execute (). Предыдущие версии 
    pyodbc (2.0.x) возвращали разные значения, но версии 2.1+ всегда возвращали сам курсор.
    Это учитывает компактный код, такой как:
'''
for row in cursor.execute("select album_id, photo_id from photos where user_id=1"):
    print(row.album_id, row.photo_id)

row = cursor.execute("select * from tmp").fetchone()
rows = cursor.execute("select * from tmp").fetchall()

count = cursor.execute("update photos set processed=1 where user_id=1").rowcount
count = cursor.execute("delete from photos where user_id=1").rowcount

##################################################################################################
#
# Passing Parameters (Передача параметров)
#
##################################################################################################
'''
en: As specified in the DB API, Cursor.execute() accepts an optional sequence of parameters:
ru: Как указано в API БД, Cursor.execute () принимает необязательную последовательность параметров:
'''
cursor.execute("select a from tbl where b=? and c=?", (x, y))
'''
en: However, this seems complicated for something as simple as passing parameters, so pyodbc also 
    accepts the parameters directly. Note in this example that x & y are not in a tuple:
ru: Однако это кажется сложным для чего-то такого простого, как передача параметров, поэтому 
    pyodbc также принимает параметры напрямую. Обратите внимание, что в этом примере x & y не 
    находятся в кортеже:
'''
cursor.execute("select a from tbl where b=? and c=?", x, y)

##################################################################################################
#
# Autocommit Mode (Режим автоматической фиксации)
#
##################################################################################################
'''
en: The DB API specifies that connections require a manual commit and pyodbc complies with this. 
    However, connections also support autocommit, using the autocommit keyword of the connection 
    function or the autocommit attribute of the Connection object:
ru: DB API указывает, что соединения требуют фиксации вручную, и pyodbc соответствует этому. 
    Однако соединения также поддерживают автокоммит, используя ключевое слово autocommit функции 
    соединения или атрибут autocommit объекта Connection:
'''
cnxn = pyodbc.connect(cstring, autocommit=True)
# or
cnxn.autocommit = True
cnxn.autocommit = False

##################################################################################################
#
# Lowercase (Строчные)
#
##################################################################################################
'''
en: Setting pyodbc.lowercase=True will cause all column names in rows to be lowercased. Some 
    people find this easier to work with, particularly if a database has a mix of naming 
    conventions. If your database is case-sensitive, however, it can cause some confusion.
ru: Установка pyodbc.lowercase=True приведет к тому, что имена всех столбцов в строках будут в нижнем 
    регистре. Некоторые люди считают, что с этим легче работать, особенно если в базе данных есть 
    сочетание соглашений об именах. Однако, если ваша база данных чувствительна к регистру, это 
    может вызвать путаницу.
'''

##################################################################################################
#
# Connection Pooling (Пул подключений)
#
##################################################################################################
'''
en: ODBC connection pooling is turned on by default. It can be turned off by setting 
    pyodbc.pooling=False before any connections are made.
ru: Пул соединений ODBC включен по умолчанию. Его можно отключить, установив pyodbc.pooling=False 
    перед установлением каких-либо подключений.
'''

##################################################################################################
#
# Query Timeouts (Тайм-ауты запроса)
#
##################################################################################################
'''
en: The Connection.timeout attribute can be set to a number of seconds after which a query should 
    raise an error. The value is in seconds and will cause an OperationalError to be raised with 
    SQLSTATE HYT00 or HYT01. By default the timeout value is 0 which disables the timeout.
ru: Атрибут Connection.timeout может быть установлен на количество секунд, после которого запрос 
    должен вызвать ошибку. Значение указывается в секундах и приведет к возникновению ошибки 
    OperationalError с помощью SQLSTATE HYT00 или HYT01. По умолчанию значение времени ожидания 
    равно 0, что отключает время ожидания.
'''

##################################################################################################
#
# Miscellaneous ODBC Functions (Разные функции ODBC)
#
##################################################################################################
'''
en: Most of the ODBC catalog functions are available as methods on Cursor objects. The results are 
    presented as SELECT results in rows that are fetched normally. The Cursor page documents these, 
    but it may be helpful to refer to Microsoft's ODBC documentation for more details.
    For example:
ru: Большинство функций каталога ODBC доступны как методы для объектов Cursor. Результаты 
    представлены как результаты SELECT в строках, которые выбираются нормально. Страница Cursor 
    документирует это, но может быть полезно обратиться к документации ODBC от Microsoft для 
    получения более подробной информации.
    Например:
'''
cnxn = pyodbc.connect(...)
cursor = cnxn.cursor()
for row in cursor.tables():
    print(row.table_name)
# +====================+=======================+=================================================+
# | ODBC Function      | Method                |	Description 	                             |
# +====================+=======================+=================================================+
# | SQLTables          | Cursor.tables         | Returns a list of table, catalog, or schema     |
# |                    |                       | names, and table types.                         |
# +--------------------+-----------------------+-------------------------------------------------+
# | SQLColumns         | Cursor.columns        | Returns a list of column names in specified     |
# |                    |                       | tables.                                         |
# +--------------------+-----------------------+-------------------------------------------------+
# | SQLStatistics      | Cursor.statistics     |Returns a list of statistics about a single table|
# |                    |                       | and the indexes associated with the table.      |
# +--------------------+-----------------------+-------------------------------------------------+
# | SQLSpecialColumns  | Cursor.rowIdColumns   | Returns a list of columns that uniquely identify|
# |                    |                       | a row.                                          |
# +--------------------+-----------------------+-------------------------------------------------+
# | SQLSpecialColumns  | Cursor.rowVerColumns  | Returns a list of columns that are              |
# |                    |                       | automatically updated any any value in the      |
# |                    |                       | row is updated.                                 |
# +--------------------+-----------------------+-------------------------------------------------+
# | SQLPrimaryKeys     | Cursor.primaryKeys    | Returns a list of column names that make up     |
# |                    |                       | the primary key for a table.                    |
# +--------------------+-----------------------+-------------------------------------------------+
# | SQLForeignKeys     | Cursor.foreignKeys    | Returns a list of column names that are         |
# |                    |                       | foreign keys in the specified table (columns in |
# |                    |                       | the specified table that refer to primary keys  |
# |                    |                       | in other tables) or foreign keys in other tables|
# |                    |                       | that refer to the primary key in the specified  |
# |                    |                       | table.                                          |
# +--------------------+-----------------------+-------------------------------------------------+
# | SQLProcedures      | Cursor.procedures     | Returns information about the procedures in     |
# |                    |                       | the data source.                                |
# +--------------------+-----------------------+-------------------------------------------------+
# | SQLProcedures      | Cursor.getTypeInfo    | Returns a information about the specified       |
# |                    |                       | data type or all data types supported by the    |
# |                    |                       | driver.                                         |
# +--------------------+-----------------------+-------------------------------------------------+


##################################################################################################
##################################################################################################
###
#                               Driver support for fast_executemany
###                            (Поддержка драйверов для fast_executemany)
##################################################################################################
##################################################################################################

'''
en: Not all ODBC drivers support parameter arrays, the internal ODBC mechanism that 
    fast_executemany uses to do its magic. If you have additional or updated information on a 
    particular driver please feel free to update this list.
ru: Не все драйверы ODBC поддерживают массивы параметров , внутренний механизм ODBC, который 
    fast_executemany использует для своей магии. Если у вас есть дополнительная или обновленная 
    информация о конкретном драйвере, пожалуйста, не стесняйтесь обновлять этот список.
'''
# +============+====================+===============+==============+========+=========+
# | Database   | Driver Name        | Driver Version| OS           | Result | Notes   |
# |            |                    |	 	        |              |        |         |
# +============+====================+===============+==============+========+=========+
# | Microsoft  | Microsoft Access   | Access        | Windows      | crashes|         |
# | Access     | Driver (*.mdb,     | 2010          |              | CPython|         |
# |            | *.accdb)           |               |              |        |         |
# +------------+--------------------+---------------+--------------+--------+---------+
# | Microsoft  | ODBC Driver 17 for | 17.x          | Linux/Windows| works  |local    |
# | SQL Server | SQL Server         |               |              |        |temporary|
# |            |                    |               |              |        |tables,  |
# |            |                    |               |              |        |TVPs     |
# +------------+--------------------+---------------+--------------+--------+---------+
# | Microsoft  | FreeTDS            | 01.00.0082    | Linux        | crashes|         |
# | SQL Server | (libtdsodbc.so)    |               |              | CPython|         |
# +------------+--------------------+---------------+--------------+--------+---------+
# | MySQL      | MySQL ODBC 8.0     | 8.0.12        | Windows      | no     |         |
# |            | Unicode Driver     |               |              | effect |         |
# +------------+--------------------+---------------+--------------+--------+---------+
# | PostgreSQL | PostgreSQL         | 11.01.00      | Windows      | no     |         |
# |            | Unicode(x64)       |               |              | effect |         |
# +------------+--------------------+---------------+--------------+--------+---------+
'''
en: "No effect" means that enabling fast_executemany does not seem to cause problems, 
    but it also doesn't make any significant difference to the amount of network traffic 
    generated by an executemany call.
ru: «Нет эффекта» означает, что включение fast_executemany , по-видимому, не вызывает проблем, 
    но также не оказывает существенного влияния на объем сетевого трафика, генерируемого 
    executemany .
'''

##################################################################################################
##################################################################################################
###
#                               Tips and Tricks by Database Platform
###                             Советы и рекомендации по платформе базы данных
##################################################################################################
##################################################################################################

'''
en: The following is a collection of tips for working with specific database platforms. For 
    additional tips that apply to pyodbc as a whole, see Features beyond the DB API.
ru: Ниже приведен набор советов по работе с конкретными платформами баз данных. Дополнительные 
    советы, относящиеся к pyodbc в целом, см. В разделе Возможности помимо API БД .
'''

##################################################################################################
#
# Microsoft Access
#
##################################################################################################


#  _______________________________________________________________________________________________
# |
# | Databases containing ODBC Linked Tables (Базы данных, содержащие ODBC связанные таблицы)
# |_______________________________________________________________________________________________

'''
en: pyodbc can work with Access databases that contain ODBC Linked Tables if we disable connection 
    pooling before connecting to the Access database:
ru: pyodbc может работать с базами данных Access, которые содержат связанные таблицы ODBC, если мы 
    отключаем пул соединений перед подключением к базе данных Access:
'''
import pyodbc

pyodbc.pooling = False
cnxn = pyodbc.connect(r"DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};DBQ= ... ")

#  _______________________________________________________________________________________________
# |
# | Creating a new database file (Создание нового файла базы данных)
# |_______________________________________________________________________________________________

'''
en: Access DDL does not support CREATE DATABASE (or similar). If you need to create a new, empty 
    database file you can use the (free) third-party msaccessdb module.
ru: Access DDL не поддерживает CREATE DATABASE (или аналогичную). Если вам нужно создать новый 
    пустой файл базы данных, вы можете использовать (бесплатный) сторонний модуль msaccessdb .
'''

#  _______________________________________________________________________________________________
# |
# | Limitations of the Access ODBC driver (Ограничения драйвера ODBC для доступа)
# |_______________________________________________________________________________________________

'''
en: The Access ODBC driver is not able to fully support some of the "complex" column types like 
    "(multi-valued) Lookup" fields and "Attachment" fields. In some cases the ODBC driver can read 
    values from such a column, but is unable to perform INSERT/UPDATE/DELETE operations. If you 
    have to perform such tasks then you may need to use Access DAO (Data Access Objects), perhaps 
    in conjunction with IronPython or Python for .NET.
    There is a list of more general Microsoft Access specifications and limitations here.
ru: Драйвер ODBC для доступа не может полностью поддерживать некоторые из «сложных» типов столбцов, 
    например поля «(многозначный) поиск» и «вложение». В некоторых случаях драйвер ODBC может 
    считывать значения из такого столбца, но не может выполнять операции INSERT / UPDATE / DELETE. 
    Если вам приходится выполнять такие задачи, вам может потребоваться использовать Access DAO 
    (объекты доступа к данным), возможно, в сочетании с IronPython или Python для .NET .
    Здесь приведен список более общих спецификаций и ограничений Microsoft Access.
'''

#  _______________________________________________________________________________________________
# |
# | UnicodeDecodeError when calling Cursor.columns (Ограничения драйвера ODBC для доступа)
# |_______________________________________________________________________________________________

'''
en: There is an issue with the Access ODBC driver that can cause a UnicodeDecodeError when trying 
    to use the Cursor.columns method if the table definition in Access includes optional column 
    "Description" information:
ru: Существует проблема с драйвером ODBC Access, которая может вызвать UnicodeDecodeError при 
    попытке использовать метод Cursor.columns, если определение таблицы в Access включает 
    необязательный столбец «Описание»:
'''
Field
Name
Data
Type
Description
----------  ----------  --------------------
ID
AutoNumber
identity
primary
key
LastName
Text
Family
name
FirstName
Text
Given
name(s)
DOB
Date / Time
Date
of
Birth

# For a discussion of the issue and possible workarounds, see issue #328.
# Обсуждение проблемы и возможных обходных путей см. В выпуске № 328.


##################################################################################################
#
# Microsoft SQL Server
#
##################################################################################################


#  _______________________________________________________________________________________________
# | Authenticate using an Access Token from Azure Active Directory
# | (Проверка подлинности с использованием токена доступа из Azure Active Directory)
# |_______________________________________________________________________________________________

'''
en: The ODBC Driver for SQL Server, in addition to the authentication methods built-in to the 
    driver such as username/password, Windows Integrated (Kerberos), Azure Active Directory 
    Password, Managed Service Identity, etc., also supports authenticating to Azure SQL and DW 
    directly using an access token obtained from any of the authentication methods provided by 
    Azure Active Directory; since pyODBC 4.0.24, this can be specified as the driver-specific 
    pre-connect attribute 1256 using the following example code to supply the token in the correct 
    format:
ru: Драйвер ODBC для SQL Server, помимо встроенных в драйвер методов аутентификации, таких как имя 
    пользователя / пароль, Windows Integrated (Kerberos), пароль Azure Active Directory, 
    идентификатор управляемой службы и т. Д., Также поддерживает аутентификацию в Azure SQL и DW 
    напрямую, используя токен доступа, полученный любым из методов аутентификации, предоставляемых 
    Azure Active Directory; начиная с pyODBC 4.0.24, это может быть указано как специфичный для 
    драйвера атрибут 1256 предварительного подключения с использованием следующего примера кода для 
    предоставления токена в правильном формате:
'''
# (Python 2.x)
token = "eyJ0eXAiOi...";
exptoken = "";
for i in token:
    exptoken += i;
    exptoken += chr(0);
tokenstruct = struct.pack("=i", len(exptoken)) + exptoken;
conn = pyodbc.connect(connstr, attrs_before={1256: bytearray(tokenstruct)});

# (Python 3.x)
token = b"eyJ0eXAiOi...";
exptoken = b"";
for i in token:
    exptoken += bytes({i});
    exptoken += bytes(1);
tokenstruct = struct.pack("=i", len(exptoken)) + exptoken;
conn = pyodbc.connect(connstr, attrs_before={1256: tokenstruct});

#  _______________________________________________________________________________________________
# | Stored Procedures with output parameters and/or return values
# | (Хранимые процедуры с выходными параметрами и / или возвращаемыми значениями)
# |_______________________________________________________________________________________________

'''
en: See the Calling Stored Procedures page for an example of how to use a bit of T-SQL to retrieve 
    these values.
ru: См. Страницу « Вызов хранимых процедур» для примера того, как использовать бит T-SQL для 
    получения этих значений.
'''

#  _______________________________________________________________________________________________
# | Connecting to a named instance of SQL Server from a Linux client
# | (Подключение к именованному экземпляру SQL Server из клиента Linux)
# |_______________________________________________________________________________________________

'''
en: Microsoft's SQL Server ODBC Driver for Linux is unable to resolve SQL Server instance names. 
    However, if the SQL Browser service is running on the target machine we can use the (free) 
    third-party sqlserverport module to look up the TCP port based on the instance name.
ru: Драйвер Microsoft ODBC для SQL Server для Linux не может разрешить имена экземпляров SQL Server. 
    Однако, если служба SQL Browser работает на целевой машине, мы можем использовать (бесплатный) 
    сторонний модуль sqlserverport для поиска порта TCP на основе имени экземпляра.
'''

#  _______________________________________________________________________________________________
# | DATETIMEOFFSET columns (e.g., "ODBC SQL type -155 is not yet supported")
# | (Столбцы DATETIMEOFFSET (например, «Тип ODBC SQL -155 еще не поддерживается»))
# |_______________________________________________________________________________________________

'''
en: Use an Output Converter function to retrieve such values. See the examples on the Using an Output 
    Converter function wiki page.
    Query parameters for DATETIMEOFFSET columns currently must be sent as strings. Note that SQL 
    Server is rather fussy about the format of the string:
ru: Используйте функцию Output Converter для получения таких значений. Смотрите примеры на 
    вики-странице Использование функции Output Converter .
    Параметры запроса для столбцов DATETIMEOFFSET в настоящее время должны отправляться в виде 
    строк. Обратите внимание, что SQL Server довольно суетлив относительно формата строки:
'''
# sample data
dto = datetime(2018, 8, 2, 0, 28, 12, tzinfo=timezone(timedelta(hours=-6)))

dto_string = dto.strftime("%Y-%m-%d %H:%M:%S %z")  # 2018-08-02 00:28:12 -0600
# Trying to use the above will fail with
#   "Conversion failed when converting date and/or time from character string."
# We need to add the colon for SQL Server to accept it
dto_string = dto_string[:23] + ":" + dto_string[23:]  # 2018-08-02 00:28:12 -06:00

#  _______________________________________________________________________________________________
# |
# | TIME columns (Столбцы ВРЕМЯ)
# |_______________________________________________________________________________________________

'''
en: Due to legacy considerations, pyodbc uses the ODBC TIME_STRUCT structure for datetime.time 
    query parameters. TIME_STRUCT does not understand fractional seconds, so datetime.time values 
    have their fractional seconds truncated when passed to SQL Server.
ru: Из-за устаревших соображений pyodbc использует структуру ODBC TIME_STRUCT для параметров 
    запроса datetime.time . TIME_STRUCT не TIME_STRUCT доли секунды, поэтому значения datetime.time 
    TIME_STRUCT их доли секунды при передаче в SQL Server.
'''
crsr.execute("CREATE TABLE #tmp (id INT, t TIME)")
t = datetime.time(hour=12, minute=23, second=34, microsecond=567890)
crsr.execute("INSERT INTO #tmp (id, t) VALUES (1, ?)", t)
rtn = crsr.execute("SELECT CAST(t AS VARCHAR) FROM #tmp WHERE id=1").fetchval()
print(rtn)  # 12:23:34.0000000
'''
en: The workaround is to pass the query parameter as a string
ru: Обходной путь должен передать параметр запроса как строку
'''
crsr.execute("INSERT INTO #tmp (id, t) VALUES (1, ?)", str(t))
rtn = crsr.execute("SELECT CAST(t AS VARCHAR) FROM #tmp WHERE id=1").fetchval()
print(rtn)  # 12:23:34.5678900
'''
en: Note that TIME columns retrieved by pyodbc have their microseconds intact
ru: Обратите внимание, что столбцы TIME, полученные с помощью pyodbc, имеют свои микросекунды без 
    изменений
'''
rtn = crsr.execute("SELECT t FROM #tmp WHERE id=1").fetchval()
print(repr(rtn))  # datetime.time(12, 23, 34, 567890)

#  _______________________________________________________________________________________________
# | SQL Server Numeric Precision vs. Python Decimal Precision
# | (Числовая точность SQL Server против десятичной точности Python)
# |_______________________________________________________________________________________________

'''
en: Python's decimal.Decimal type can represent floating point numbers with greater than 35 digits 
    of precision, which is the maximum supported by SQL server. Binding parameters that exceed 
    this precision will result in an invalid precision error from the driver 
    ("HY104 [Microsoft][...]Invalid precision value").
ru: Тип десятичного числа в Python. Может представлять числа с плавающей запятой с точностью более 
    35 цифр, что является максимальным значением, поддерживаемым сервером SQL. Параметры привязки, 
    превышающие эту точность, приведут к ошибке недопустимой точности драйвера 
    («HY104 [Microsoft] [...] Недопустимое значение точности»).
'''

#  _______________________________________________________________________________________________
# | Using fast_executemany with a #temporary table
# | (Использование fast_executemany с #teventr)
# |_______________________________________________________________________________________________

'''
en: fast_executemany can have difficulty identifying the column types of a local #temporary table 
    under some circumstances (#295).
ru: fast_executemany некоторых обстоятельствах fast_executemany может быть трудно определить типы 
    столбцов локальной таблицы #tever ( # 295 ).
'''

# **********
#  -- Workaround 1: ODBC Driver 17 for SQL Server and UseFMTOnly=yes or ColumnEncryption=Enabled
# **********

'''
en: Use "ODBC Driver 17 for SQL Server" (or newer) and include UseFMTOnly=yes or 
    ColumnEncryption=Enabled in the connection string, e.g.,
ru: Используйте «Драйвер ODBC 17 для SQL Server» (или новее) и UseFMTOnly=yes или 
    ColumnEncryption=Enabled в строке подключения, например,
'''
cnxn_str = (
    "Driver=ODBC Driver 17 for SQL Server;"
    "Server=192.168.1.144,49242;"
    "UID=sa;PWD=_whatever_;"
    "Database=myDb;"
    "UseFMTOnly=yes;"
)
# or
cnxn_str = (
    "Driver=ODBC Driver 17 for SQL Server;"
    "Server=192.168.1.144,49242;"
    "UID=sa;PWD=_whatever_;"
    "Database=myDb;"
    "ColumnEncryption=Enabled;"
)

# **********
#  -- Workaround 2: pyodbc 4.0.24 and Cursor.setinputsizes
# **********

'''
en: Upgrade to pyodbc 4.0.24 (or newer) and use setinputsizes to specify the parameter type, etc..
ru: Обновление до pyodbc 4.0.24 (или новее) и использование setinputsizes для указания типа 
    параметра и т. Д.
'''
crsr.execute("""\
CREATE TABLE #issue295 (
    id INT IDENTITY PRIMARY KEY, 
    txt NVARCHAR(50), 
    dec DECIMAL(18,4)
    )""")
sql = "INSERT INTO #issue295 (txt, dec) VALUES (?, ?)"
params = [('Ώπα', 3.141)]
# explicitly set parameter type/size/precision
crsr.setinputsizes([(pyodbc.SQL_WVARCHAR, 50, 0), (pyodbc.SQL_DECIMAL, 18, 4)])
crsr.fast_executemany = True
crsr.executemany(sql, params)

# **********
#  -- Workaround 3: Use a global ##temporary table
# **********

'''
en: If neither of the previous workarounds is feasible, simply use a global ##temporary table 
    instead of a local #temporary table.
ru: Если ни один из предыдущих обходных путей не выполним, просто используйте глобальную 
    временную таблицу ## вместо локальной таблицы #teilitary.
'''

#  _______________________________________________________________________________________________
# | Always Encrypted
# | (Всегда зашифрован)
# |_______________________________________________________________________________________________

'''
en: SQL Server 2016 and later support client-side encryption and decryption of data, when used with 
    the ODBC Driver 13.1 for SQL Server or later. Retrieving data from encrypted columns through 
    pyODBC has no special considerations; however, inserting data into encrypted columns is subject 
    to some limitations of the ODBC driver itself, the two most important in pyODBC usage being:
    * Data to be inserted into or compared with encrypted columns must be passed through a 
      parameterised query; literals in the T-SQL directly will not work.
    * Due to the lack of server-side type conversion, it may be necessary to use the setinputsizes() 
      function extension (see https://github.com/mkleehammer/pyodbc/pull/280 for details) on 
      specific columns to cause pyODBC to send the correct data type to the server for insertion or 
      comparison with encrypted data. If you receive 'Operand type clash' errors, this is likely 
      to be the case.
ru: SQL Server 2016 и более поздние версии поддерживают шифрование и дешифрование данных на стороне 
    клиента при использовании с драйвером ODBC 13.1 для SQL Server или более поздней версии. 
    Извлечение данных из зашифрованных столбцов через pyODBC не имеет особых соображений; тем не 
    менее, вставка данных в зашифрованные столбцы зависит от некоторых ограничений самого драйвера 
    ODBC, два наиболее важных при использовании pyODBC:
    * Данные, которые должны быть вставлены или сопоставлены с зашифрованными столбцами, должны 
      передаваться через параметризованный запрос; Литералы в T-SQL напрямую работать не будут.
    * Из-за отсутствия преобразования типов на стороне сервера может потребоваться использовать 
      расширение функции setinputsizes() (см. Https://github.com/mkleehammer/pyodbc/pull/280 ) в 
      определенных столбцах, чтобы pyODBC мог отправьте правильный тип данных на сервер для вставки 
      или сравнения с зашифрованными данными. Если вы получаете сообщения об ошибках типа 
      «Операнд», это, вероятно, имеет место.
'''

##################################################################################################
##################################################################################################
###
#                               Using an Output Converter function
###                             Использование функции Output Converter
##################################################################################################
##################################################################################################

'''
en: Output Converter functions offer a flexible way to work with returned results that pyodbc does 
    not natively support. For example, Microsoft SQL Server returns values from a DATETIMEOFFSET 
    column as SQL type -155, which does not have native support in pyodbc (and many other ODBC 
    libraries). Simply retrieving the value ...
ru: Функции конвертера вывода предоставляют гибкий способ работы с возвращаемыми результатами, 
    которые pyodbc изначально не поддерживает. Например, Microsoft SQL Server возвращает значения 
    из столбца DATETIMEOFFSET как тип SQL -155, который не имеет встроенной поддержки в pyodbc 
    (и многих других библиотеках ODBC). Просто получить значение ...
'''
import pyodbc

cnxn = pyodbc.connect("DSN=myDb")

crsr = cnxn.cursor()
# create test data
crsr.execute("CREATE TABLE #dto_test (id INT PRIMARY KEY, dto_col DATETIMEOFFSET)")
crsr.execute("INSERT INTO #dto_test (id, dto_col) VALUES (1, '2017-03-16 10:35:18 -06:00')")

value = crsr.execute("SELECT dto_col FROM #dto_test WHERE id=1").fetchval()
print(value)

crsr.close()
cnxn.close()
# ... results in the error:
# ... приводит к ошибке:
pyodbc.ProgrammingError: ('ODBC SQL type -155 is not yet supported. column-index=0 type=-155', 'HY106')
'''
en: However, we can define an Output Converter function to decode the bytes returned, add that 
    function to the Connection object, and then use the resulting value, e.g.,
ru: Тем не менее, мы можем определить функцию Output Converter для декодирования возвращаемых 
    байтов, добавить эту функцию в объект Connection и затем использовать полученное значение, 
    например,
'''
import struct
import pyodbc

cnxn = pyodbc.connect("DSN=myDb")


def handle_datetimeoffset(dto_value):
    # ref: https://github.com/mkleehammer/pyodbc/issues/134#issuecomment-281739794
    tup = struct.unpack("<6hI2h", dto_value)  # e.g., (2017, 3, 16, 10, 35, 18, 0, -6, 0)
    tweaked = [tup[i] // 100 if i == 6 else tup[i] for i in range(len(tup))]
    return "{:04d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}.{:07d} {:+03d}:{:02d}".format(*tweaked)


crsr = cnxn.cursor()
# create test data
crsr.execute("CREATE TABLE #dto_test (id INT PRIMARY KEY, dto_col DATETIMEOFFSET)")
crsr.execute("INSERT INTO #dto_test (id, dto_col) VALUES (1, '2017-03-16 10:35:18 -06:00')")

cnxn.add_output_converter(-155, handle_datetimeoffset)
value = crsr.execute("SELECT dto_col FROM #dto_test WHERE id=1").fetchval()
print(value)

crsr.close()
cnxn.close()

# which prints the string representation of the DATETIMEOFFSET value
# который печатает строковое представление значения DATETIMEOFFSET
2017 - 03 - 16
10: 35:18.0000000 - 06: 00


# Or, we could use this function to create a datetime object
# Или мы могли бы использовать эту функцию для создания объекта datetime
def handle_datetimeoffset(dto_value):
    # ref: https://github.com/mkleehammer/pyodbc/issues/134#issuecomment-281739794
    tup = struct.unpack("<6hI2h", dto_value)  # e.g., (2017, 3, 16, 10, 35, 18, 0, -6, 0)
    return datetime(tup[0], tup[1], tup[2], tup[3], tup[4], tup[5], tup[6] // 1000,
                    timezone(timedelta(hours=tup[7], minutes=tup[8])))


# which would return:
# который бы вернулся:
datetime.datetime(2017, 3, 16, 10, 35, 18, 0, tzinfo=datetime.timezone(datetime.timedelta(-1, 64800)))

##################################################################################################
#
# Removing an Output Converter function
# (Удаление функции выходного преобразователя)
##################################################################################################

# To remove all Output Converter functions, simply do:
# Чтобы удалить все функции Output Converter, просто выполните:
cnxn.clear_output_converters()
'''
en: To remove a single Output Converter function (new in version 4.0.25), use remove_output_converter 
    like so:
ru: Чтобы удалить одну функцию Конвертер вывода (новая в версии 4.0.25), используйте 
    remove_output_converter следующим образом:
'''
cnxn.add_output_converter(pyodbc.SQL_WVARCHAR, decode_sketchy_utf16)
rows = crsr.columns("Clients").fetchall()
cnxn.remove_output_converter(pyodbc.SQL_WVARCHAR)  # restore default behaviour

##################################################################################################
#
# Temporarily replacing an Output Converter function
# (Временная замена функции выходного преобразователя)
##################################################################################################
'''
en: Starting with version 4.0.26 we can also use get_output_converter to retrieve the currently 
    active Output Converter function so we can temporarily replace it and then restore it 
    afterwards.
ru: Начиная с версии 4.0.26, мы также можем использовать get_output_converter для извлечения 
    активной в данный момент функции Output Converter, чтобы мы могли временно заменить ее и затем 
    восстановить.
'''
prev_converter = cnxn.get_output_converter(pyodbc.SQL_WVARCHAR)
cnxn.add_output_converter(pyodbc.SQL_WVARCHAR, decode_sketchy_utf16)  # temporary replacement
rows = crsr.columns("Clients").fetchall()
cnxn.add_output_converter(pyodbc.SQL_WVARCHAR, prev_converter)  # restore previous behaviour

##################################################################################################
##################################################################################################
###
#                               Drivers and Driver Managers
###                             Водители и менеджеры водителей
##################################################################################################
##################################################################################################


##################################################################################################
#
# How All the Pieces Fit Together
# (Как все кусочки сочетаются друг с другом)
##################################################################################################

'''
en: The following diagram attempts to describe how your Python code communicates with the target 
    database:
ru: Следующая диаграмма пытается описать, как ваш код Python взаимодействует с целевой базой 
    данных:
'''
--- Your
Server - -------------
| |
| your
Python
code |
| | |
| pyodbc |
| | |
| driver
manager |
| | |
| driver |
| | |
------------------------------
|
(network connection)
|
--- Database
Server - ---------
| | |
| database |
| |
------------------------------
'''
en: As you can see, there are a series of intermediary interfaces between your Python code and the 
    target database. Your Python code calls functions in the pyodbc Python package which in turn 
    communicates with a "driver manager". The driver manager provides the API that (should) conform 
    to the ODBC standard. For example, to open a connection to a database pyodbc calls the 
    SQLDriverConnect function in the driver manager, and to execute a SQL statement pyodbc calls 
    SQLExecDirect. The driver manager then calls the database "driver" which in turn interacts 
    directly with the database, typically across a network, marshalling instructions and data back 
    and forth between your server and the database server. Hence, if the driver manager, driver, 
    and database all conform to the ODBC standards, it should be possible to use pyodbc with any 
    database vendor. However, in reality, they all have their own particular quirks and 
    idiosyncracies, and often these are not compliant with ODBC. Pyodbc attempts to work around 
    these quirks as much as it can.
    On both Unix and Windows systems, the driver manager and driver are often rolled into the same 
    software and referred to simply as "the driver", especially when they are created by the 
    manufacturer of the RDBMS.
ru: Как видите, между вашим кодом Python и целевой базой данных существует ряд промежуточных 
    интерфейсов. Ваш код Python вызывает функции в пакете Pyodbc Python, который, в свою очередь, 
    связывается с «диспетчером драйверов». Диспетчер драйверов предоставляет API, который (должен) 
    соответствовать стандарту ODBC . Например, чтобы открыть соединение с базой данных, pyodbc 
    вызывает функцию SQLDriverConnect в диспетчере драйверов, а для выполнения инструкции SQL 
    pyodbc вызывает SQLExecDirect . Затем диспетчер драйверов вызывает базу данных как «драйвер», 
    который, в свою очередь, напрямую взаимодействует с базой данных, обычно через сеть, 
    упорядочивая инструкции и данные между вашим сервером и сервером базы данных. Следовательно, 
    если диспетчер драйверов, драйвер и база данных все соответствуют стандартам ODBC, должна быть 
    возможность использовать pyodbc с любым поставщиком базы данных. Однако в действительности все 
    они имеют свои специфические особенности и особенности, и часто они не соответствуют ODBC. 
    Pyodbc пытается обойти эти причуды как можно больше.
    Как в системах Unix, так и в Windows диспетчер драйверов и драйвер часто объединяются в одно и 
    то же программное обеспечение и называются просто «драйвером», особенно когда они создаются 
    производителем СУБД.
'''

##################################################################################################
#
# Driver Managers
# (Менеджеры водителей)
##################################################################################################

'''
en: The driver manager that pyodbc uses is determined when pyodbc is installed (through the 
    setup.py script). If you need to change the driver manager, you have to re-install pyodbc.
    Common third-party driver managers on Unix are as follows:
ru: Диспетчер драйверов, который использует pyodbc, определяется при установке pyodbc (через 
    скрипт setup.py ). Если вам нужно изменить диспетчер драйверов, вам нужно переустановить 
    pyodbc.
    Обычные сторонние менеджеры драйверов в Unix:
'''

#  _______________________________________________________________________________________________
# |
# | unixODBC
# |_______________________________________________________________________________________________

'''
en: unixODBC tries to be the definitive standard for ODBC on non-Windows platforms. It is available 
    on all Unix OS's and Mac OSX (brew install unixodbc). It also provides the command line tools 
    odbcinst and isql which are useful for manipulating the ODBC configuration files (see below) and 
    making SQL calls without pyodbc. For documentation about these utilities, best run man odbcinst 
    or man isql.
    The website for unixODBC is here http://www.unixodbc.org, but it's worth noting this website is 
    not particularly well maintained so don't look to it for extensive documentation. However, the 
    front page is very useful for the release notes about unixODBC itself.
    Since version 3.0.8 (April 2015), pyodbc has assumed unixODBC is being used rather than iODBC 
    (on Unix platforms).
ru: unixODBC пытается быть окончательным стандартом для ODBC на платформах, отличных от Windows. 
    Он доступен на всех ОС Unix и Mac OSX ( brew install unixodbc ). Он также предоставляет 
    инструменты командной строки odbcinst и isql которые полезны для управления файлами конфигурации 
    ODBC (см. Ниже) и выполнения вызовов SQL без pyodbc. Для документации об этих утилитах лучше 
    всего запустить man odbcinst или man isql .
    Веб-сайт для unixODBC находится здесь http://www.unixodbc.org , но стоит отметить, что этот 
    веб-сайт не очень хорошо поддерживается, поэтому не обращайте на него внимания за обширной 
    документацией. Тем не менее, первая страница очень полезна для заметок о выпуске о самом unixODBC.
    Начиная с версии 3.0.8 (апрель 2015 г.), pyodbc предполагал, что используется unixODBC, а не 
    iODBC (на платформах Unix).
'''

#  _______________________________________________________________________________________________
# |
# | iODBC
# |_______________________________________________________________________________________________

'''
en: iODBC is another driver manager for Unix platforms. It used to be pre-installed as the default 
    driver manager on Mac OSX, but since around early 2015, this is no longer the case.
ru: iODBC - еще один менеджер драйверов для платформ Unix. Раньше он был предварительно установлен 
    в качестве менеджера драйверов по умолчанию на Mac OSX, но с начала 2015 года это уже не так.
'''

#  _______________________________________________________________________________________________
# |
# | Drivers
# |_______________________________________________________________________________________________

'''
en: Drivers are so specific to a RDBMS they are typically written by the manufacturer of the RDBMS. 
    Sometimes, however, there are third-party drivers available for certain RDBMS's. For example, 
    FreeTDS works with SQL Server and Sybase.
ru: Драйверы настолько специфичны для СУБД, что обычно пишутся производителем СУБД. Однако иногда 
    для определенных СУБД доступны драйверы сторонних производителей. Например, FreeTDS работает с 
    SQL Server и Sybase.
'''

##################################################################################################
#
# ODBC Configuration Files odbc.ini and odbcinst.ini (unix only)
# (Файлы конфигурации ODBC odbc.ini и odbcinst.ini (только для Unix))
##################################################################################################

'''
en: The driver manager has to be told what drivers and databases are available on your server. This 
    information is stored in the odbcinst.ini and odbc.ini files, respectively.
    Information about ODBC drivers on a server is stored in the odbcinst.ini file. It contains a 
    catalog of the drivers which have been installed, including the familiar names by which they are 
    referenced, e.g.:
ru: Диспетчер драйверов должен сказать, какие драйверы и базы данных доступны на вашем сервере. Эта 
    информация хранится в файлах odbcinst.ini и odbc.ini соответственно.
    Информация о драйверах ODBC на сервере хранится в файле odbcinst.ini . Он содержит каталог 
    драйверов, которые были установлены, включая знакомые имена, на которые они ссылаются, например:
'''
[ODBC Driver 11
for SQL Server]
Description = Microsoft
ODBC
Driver
11
for SQL Server
    Driver = / opt / microsoft / msodbcsql / lib64 / libmsodbcsql - 11.0.so
    .2270
    .0
Threading = 1
UsageCount = 1

[ODBC Driver
for Vertica]
Description = Vertica
ODBC
Driver
Driver = / opt / vertica / lib64 / libverticaodbc.so
UsageCount = 1

'''
en: Here, the familiar name for the SQL Server driver is "ODBC Driver 11 for SQL Server", and "ODBC 
    Driver for Vertica" for the Vertica driver. Note the driver software itself is referred to in 
    the "Driver" line. Typically, when database drivers are installed, the installation process 
    adds a new catalog entry to the odbcinst.ini file so it shouldn't normally be necessary to edit 
    this file.
    Information about database connections on your server is stored in the odbc.ini file. It contains 
    a catalog of DSN's (Data Source Names), one for each database connection. Each DSN includes a 
    driver reference in the "Driver" line, which is the driver's familiar name from the odbcinst.ini 
    file, e.g.:
ru: Здесь знакомое имя для драйвера SQL Server: «Драйвер ODBC 11 для SQL Server» и «Драйвер ODBC для 
    Vertica» для драйвера Vertica. Обратите внимание, что само программное обеспечение драйвера 
    указано в строке «Драйвер». Обычно, когда установлены драйверы базы данных, процесс установки 
    добавляет новую запись каталога в файл odbcinst.ini поэтому обычно нет необходимости редактировать 
    этот файл.
    Информация о соединениях с базой данных на вашем сервере хранится в файле odbc.ini . Он содержит 
    каталог DSN (имен источников данных), по одному на каждое соединение с базой данных. Каждый DSN 
    содержит ссылку на драйвер в строке «Драйвер», которая является знакомым именем драйвера из файла 
    odbcinst.ini , например:
'''
[Samson]
Driver = ODBC
Driver
11
for SQL Server
    Description = The
    OLTP
    server
Server = samson.xyz.com

[Delilah]
Driver = ODBC
Driver
for Vertica
    Description = The
    OLAP
    server
Server = delilah.xyz.com
Database = cube

'''
en: Typically, catalog entries to odbc.ini are added manually, either by using the odbcinst -i -s 
    command, or by editing the file directly.
    Once the two ODBC configuration files are set up correctly, connections to a database can then be 
    conveniently made using just the DSN without having to refer to a server, driver manager, or 
    driver, e.g.:
ru: Как правило, записи каталога в odbc.ini добавляются вручную, либо с помощью команды odbcinst -i -s , 
    либо путем непосредственного редактирования файла.
    После того, как два файла конфигурации ODBC настроены правильно, соединения с базой данных можно 
    удобно установить, используя только DSN, без необходимости обращаться к серверу, диспетчеру 
    драйверов или драйверу, например:
'''
conn = pyodbc.connect(' DSN = Samson; UID = mylogin; PWD = mypassword ')

'''
en: You can find out where the odbc.ini and odbcinst.ini files are on a server by running odbcinst -j. 
    Typically they are in the /etc/ directory, although they may also been stored as hidden files in 
    your home directory, i.e. ~/.odbc.ini.
ru: Вы можете узнать, где находятся файлы odbc.ini и odbcinst.ini на сервере, запустив odbcinst -j . 
    Обычно они находятся в каталоге /etc/ , хотя они также могут храниться в виде скрытых файлов в 
    вашем домашнем каталоге, то есть ~/.odbc.ini .
'''

##################################################################################################
##################################################################################################
###
#                               Building pyodbc from source
###                             Сборка pyodbc из источника
##################################################################################################
##################################################################################################


##################################################################################################
#
# Overview (обзор)
#
##################################################################################################


#  _______________________________________________________________________________________________
# |
# | Obtaining Source (Получение источника)
# |_______________________________________________________________________________________________

'''
en: The source for released versions are provided in zip files on the front page of this repository.
    If you are going to work on pyodbc (changes are welcome!), best fork the repository from github 
    so I can easily pull changes from your fork.
    If you want an unreleased version from github but don't have git installed, choose the branch and 
    commit you want and use the download button. It will offer you a tar or zip file.
ru: Исходный код выпущенных версий представлен в zip-файлах на первой странице этого репозитория.
    Если вы собираетесь работать с pyodbc (изменения приветствуются!), Лучше разветвите репозиторий 
    из github, чтобы я мог легко получать изменения с вашего форка.
    Если вам нужна неизданная версия от github, но у вас не установлен git, выберите ветку и 
    зафиксируйте ее, а затем нажмите кнопку загрузки. Он предложит вам файл tar или zip.
'''

#  _______________________________________________________________________________________________
# |
# | Compiling (составление)
# |_______________________________________________________________________________________________

'''
en: pyodbc is built using distutils, which comes with Python. If you have the appropriate C++ 
    compiler installed (see below), run the following in the pyodbc source directory:
ru: Pyodbc построен с использованием distutils , который поставляется с Python. Если у вас установлен 
    соответствующий компилятор C ++ (см. Ниже), запустите следующее в каталоге исходных кодов pyodbc:
'''
python
setup.py
build

# To install after a successful build:
# Чтобы установить после успешной сборки:

python
setup.py
install

#  _______________________________________________________________________________________________
# |
# | Version
# |_______________________________________________________________________________________________

'''
en: pyodbc uses git tags in the format major.minor.patch for versioning. If the commit being built 
    has a tag, such as "3.0.7", then that is used for the version. If the commit is not tagged, then 
    it is assumed that the commit is a beta for an upcoming release. The patch number is incremented, 
    such as "3.0.7", and the number of commits since the previous release is appended: 3.0.7-beta3.
    If you are building from inside a git repository, the build script will automatically determine 
    the correct version from the tags (using git describe).
    If you downloaded the source as a zip from the front page, the version will be in the text file 
    PGK-INFO. The build script will read the version from that text file.
    If the version cannot be determined for some reason, you will see a warning about this and the 
    script will build version 2.1.0. Do not distribute this version since you won't be able to track 
    its actual version!
ru: pyodbc использует теги git в формате major.minor.patch для управления версиями. Если создаваемый 
    коммит имеет тег, такой как «3.0.7», то он используется для версии. Если фиксация не помечена, 
    то предполагается, что фиксация является бета-версией для следующего выпуска. Номер патча 
    увеличивается, например, «3.0.7», и количество коммитов с момента добавления предыдущего выпуска: 
    3.0.7-бета3.
    Если вы строите из репозитория git, скрипт сборки автоматически определит правильную версию по 
    тегам (используя git description).
    Если вы загрузили исходный код в виде почтового индекса с первой страницы, версия будет в 
    текстовом файле PGK-INFO. Сценарий сборки будет читать версию из этого текстового файла.
    Если версия не может быть определена по какой-либо причине, вы увидите предупреждение об этом, и 
    скрипт создаст версию 2.1.0. Не распространяйте эту версию, так как вы не сможете отследить ее 
    актуальную версию!
'''

##################################################################################################
#
# Operating Systems
#
##################################################################################################


#  _______________________________________________________________________________________________
# |
# | Windows
# |_______________________________________________________________________________________________

'''
en: First of all, upgrade your Python setuptools module to the latest with pip install --upgrade 
    setuptools.
    To compile pyodbc, you must use the appropriate Microsoft Visual C++ compiler for the version of 
    Python you wish to compile. See the following wiki page for reference: 
    https://wiki.python.org/moin/WindowsCompilers
    * To build Python 2.4 or 2.5 versions, you will need the Visual Studio 2003 .NET compiler. 
      Unfortunately there is no free version of this.
    * For Python 2.6, 2.7, 3.0, 3.1 and 3.2, use the Visual C++ 2008 compiler. There is a free version 
      of this, Visual C++ 2008 Express.
    * For Python 3.3 and 3.4, use the Visual C++ 2010 compiler. There is a free version of this, 
      Visual C++ 2010 Express, although that version does not inherently allow 64-bit builds to be 
      compiled. To compile 64-bit builds with Visual C++ 2010 Express, follow these instructions: 
      http://blog.ionelmc.ro/2014/12/21/compiling-python-extensions-on-windows/
    * For Python 3.5 and 3.6, use Visual C++ 2014, VC 2017 works too.
    * For Python 3.7, use Visual C++ 2017.
    These instructions assume Windows 10. If you don't already have Visual Studio 2017 installed, 
    install Build Tools for Visual Studio 2017 by downloading from 
    https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2017 and installing 
    it. If you need to check or modify this installation, do so by opening the "Visual Studio Installer" 
    app from the Start menu. In that app under the "Products" tab, you should see 
    "Visual Studio Build Tools 2017". Click "Modify" under "Visual Studio Build Tools 2017". In the 
    new window, under the "Workloads" tab, check the small checkbox on "Visual C++ build tools". On 
    the right you'll see the "Installation details", which should now include the optional elements: 
    "Windows 10 SDK"; "Visual C++ tools for CMake"; and "Testing tools core features - Build Tools".
    To build pyodbc, from the Start menu within the "Visual Studio 2017" folder, open up an 
    "x64 Native Tools Command Prompt for VS 2017" command prompt (NOT an ordinary command prompt). 
    Make sure you open it with administrative privileges by right-clicking on the icon, choosing 
    "More" then "Run as administrator".
    Within that command prompt, cd to the top-level pyodbc directory, the one that includes setup.py, 
    then:
    1. Run python setup.py build, which creates and populates the build subdirectory.
    2. Run python setup.py bdist_wininst, which creates and populates the dist subdirectory.
    3. Run python setup.py install, which installs your new pyodbc build into your Python environment. 
       This step requires admin privileges.
    Check your new version of pyodbc by running python -c "import pyodbc; print(pyodbc.version)".
ru: Прежде всего, обновите модуль Python setuptools до последней версии с помощью pip install 
    --upgrade setuptools .
    Чтобы скомпилировать pyodbc, вы должны использовать соответствующий компилятор Microsoft Visual C ++ 
    для той версии Python, которую вы хотите скомпилировать. Смотрите следующую вики-страницу для 
    справки: https://wiki.python.org/moin/WindowsCompilers
    * Для сборки версий Python 2.4 или 2.5 вам понадобится компилятор Visual Studio 2003 .NET. К 
      сожалению, нет бесплатной версии этого.
    * Для Python 2.6, 2.7, 3.0, 3.1 и 3.2 используйте компилятор Visual C ++ 2008. Существует 
      бесплатная версия Visual C ++ 2008 Express.
    * Для Python 3.3 и 3.4 используйте компилятор Visual C ++ 2010. Существует бесплатная версия 
      Visual C ++ 2010 Express, хотя эта версия по своей сути не позволяет компилировать 64-разрядные 
      сборки. Чтобы скомпилировать 64-разрядные сборки с помощью Visual C ++ 2010 Express, выполните 
      следующие инструкции: http://blog.ionelmc.ro/2014/12/21/compiling-python-extensions-on-windows/
    * Для Python 3.5 и 3.6 используйте Visual C ++ 2014, VC 2017 тоже работает.
    * Для Python 3.7 используйте Visual C ++ 2017.
    Эти инструкции предполагают наличие Windows 10. Если у вас еще не установлена Visual Studio 2017 , 
    установите инструменты сборки для Visual Studio 2017 , загрузив с веб-сайта 
    https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio -2017 и его установка. 
    Если вам нужно проверить или изменить эту установку, сделайте это, открыв приложение 
    «Visual Studio Installer» из меню «Пуск». В этом приложении на вкладке «Продукты» вы должны 
    увидеть «Инструменты Visual Studio Build 2017». Нажмите «Изменить» в разделе «Инструменты сборки 
    Visual Studio 2017». В новом окне на вкладке «Рабочие нагрузки» установите флажок «Инструменты 
    сборки Visual C ++». Справа вы увидите «Сведения об установке», которые теперь должны включать 
    дополнительные элементы: «Windows 10 SDK»; «Инструменты Visual C ++ для CMake»; и «Основные 
    функции инструментов тестирования - Инструменты сборки».
    Чтобы создать pyodbc, в меню «Пуск» в папке «Visual Studio 2017» откройте командную строку 
    x64 Native Tools Command Prompt для VS 2017 (НЕ обычную командную строку). Убедитесь, что вы 
    открыли его с правами администратора, щелкнув правой кнопкой мыши значок, выбрав «Дополнительно», 
    а затем «Запуск от имени администратора».
    В этой командной строке cd в каталог pyodbc верхнего уровня, в который входит файл setup.py, а 
    затем:
    1. Запустите python setup.py build , который создает и заполняет подкаталог build .
    2. Запустите python setup.py bdist_wininst , который создает и заполняет подкаталог dist .
    3. Запустите python setup.py install , который установит вашу новую сборку pyodbc в вашу среду 
       Python. Этот шаг требует прав администратора.
    Проверьте вашу новую версию pyodbc, запустив python -c "import pyodbc; print(pyodbc.version)" .
'''

#  _______________________________________________________________________________________________
# |
# | Other
# |_______________________________________________________________________________________________

'''
en: To build on other operating systems, use the gcc compiler.
    On Linux, pyodbc is typically built using the unixODBC headers, so you will need unixODBC and 
    its headers installed. On a RedHat/CentOS/Fedora box, this means you would need to install 
    unixODBC-devel:
ru: Для сборки на других операционных системах используйте компилятор gcc.
    В Linux pyodbc обычно создается с использованием заголовков unixODBC, поэтому вам потребуется 
    установить unixODBC и его заголовки. На коробке RedHat / CentOS / Fedora это означает, что вам 
    необходимо установить unixODBC-devel:
'''
yum
install
unixODBC - devel

'''
en: On Fedora, you may see this error: gcc: error: /usr/lib/rpm/redhat/redhat-hardened-cc1: No such 
    file or directory. You'll need to install the redhat-rpm-config package.
ru: В Fedora вы можете увидеть эту ошибку: gcc: error: /usr/lib/rpm/redhat/redhat-hardened-cc1: No 
    such file or directory . Вам нужно будет установить пакет redhat-rpm-config .
'''
sudo
dnf
install
redhat - rpm - config

##################################################################################################
##################################################################################################
###
#                               Troubleshooting – Generating an ODBC trace log
###                             Устранение неполадок - создание журнала трассировки ODBC
##################################################################################################
##################################################################################################

'''
en: Users reporting issues with pyodbc will often be asked to provide an ODBC trace log for 
    diagnostic purposes. Fortunately, generating the log file is easy.
ru: Пользователей, сообщающих о проблемах с pyodbc, часто просят предоставить журнал трассировки 
    ODBC для диагностических целей. К счастью, создать файл журнала легко.
'''

##################################################################################################
#
# Windows
#
##################################################################################################

'''
en: Launch the 64-bit or 32-bit ODBC Administrator depending on whether you are using 64-bit or 
    32-bit Python:
    If you are running 64-bit Python, launch
ru: Запустите 64-битный или 32-битный администратор ODBC в зависимости от того, используете ли 
    вы 64-битный или 32-битный Python:
    Если вы используете 64-битный Python, запустите
'''
# C:\Windows\System32\odbcad32.exe
# If you are running 32-bit Python, launch
# Если вы используете 32-битный Python, запустите
# C:\Windows\SysWOW64\odbcad32.exe
'''
en: On the "Tracing" tab, click the "Start Tracing Now" button. Details of subsequent ODBC activity 
    will be appended to the file specified in "Log File Path".
ru: На вкладке «Отслеживание» нажмите кнопку «Начать отслеживание сейчас». Подробная информация о 
    последующих действиях ODBC будет добавлена ​​к файлу, указанному в «Путь к файлу журнала».
'''

##################################################################################################
#
# Linux
#
##################################################################################################

'''
en: To enable ODBC tracing under Linux (unixODBC), add the following two lines to the [ODBC] section 
    of odbcinst.ini.
ru: Чтобы включить трассировку ODBC в Linux (unixODBC), добавьте следующие две строки в раздел [ODBC] 
    odbcinst.ini .
'''
[ODBC]
Trace = yes
TraceFile = / tmp / odbctrace.txt
'''
en: NOTE: A bug in unixODBC 2.3.6 and earlier can interfere with logging under some ODBC drivers 
    (e.g., "ODBC Driver 17 for SQL Server"). Check your installed version of unixODBC with the 
    odbcinst -j command and upgrade if necessary.
ru: ПРИМЕЧАНИЕ. Ошибка в unixODBC 2.3.6 и более ранних версиях может помешать ведению журнала под 
    некоторыми драйверами ODBC (например, «Драйвер ODBC 17 для SQL Server»). Проверьте установленную 
    версию unixODBC с помощью команды odbcinst -j и при необходимости обновите.
'''

##################################################################################################
#
# General Notes
#
##################################################################################################

'''
en: * Always review your trace logs before submitting them. They may contain your connection string, 
      which might include the server address/port, user credentials, or other details you might not 
      want to make public.
    * Remember to turn off ODBC tracing after generating your log file. Logging ODBC activity slows 
      down ODBC operations and the log file will quickly become very large.
ru: * Всегда просматривайте журналы трассировки перед их отправкой. Они могут содержать строку вашего 
      подключения, которая может включать адрес / порт сервера, учетные данные пользователя или другие 
      сведения, которые вы, возможно, не хотите публиковать.
    * Не забудьте отключить трассировку ODBC после создания файла журнала. Ведение журнала активности 
      ODBC замедляет операции ODBC, и файл журнала быстро становится очень большим.
'''

##################################################################################################
##################################################################################################
###
#                               Running pydobc project unit tests
###                             Запуск модульных тестов проекта pydobc
##################################################################################################
##################################################################################################

'''
en: The test suite must be run with Python 2, not Python 3.
    1. python setup.py build
    2. cd into the build directory created (called "lib.something...")
    3. python ../../tests2/[name of database].py [connection information]
ru: Набор тестов должен выполняться с Python 2, а не с Python 3.
    1. python setup.py build
    2. cd в созданный каталог сборки (называется "lib.something ...")
    3. python ../../tests2/[name of database].py [connection information]
'''